package fr.pocolobo.afcepf.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.business.api.CursusIBusiness;
import fr.pocolobo.afcepf.dao.api.CursusIDao;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.entity.pedago.Cursus;

@Remote (CursusIBusiness.class)
@Stateless
public class CursusBusiness extends GenericBusiness<Cursus> implements CursusIBusiness {

   @EJB
   private CursusIDao dao;

   @Override
   public GenericIDao<Cursus> getDao() {
      return dao;
   }

   public void setDao(CursusIDao dao) {
      this.dao = dao;
   }
}
