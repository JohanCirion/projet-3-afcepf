package fr.pocolobo.afcepf.business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import fr.pocolobo.afcepf.business.api.CoursIBusiness;
import fr.pocolobo.afcepf.dao.api.CoursIDao;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.dao.api.IntervenantIDao;
import fr.pocolobo.afcepf.dao.api.MatiereIntervenantIDao;
import fr.pocolobo.afcepf.entity.pedago.Cours;
import fr.pocolobo.afcepf.entity.pedago.MatiereIntervenant;
import fr.pocolobo.afcepf.entity.utilisateurs.Intervenant;

@Remote (CoursIBusiness.class)
@Stateful
public class CoursBusiness extends GenericBusiness<Cours> implements CoursIBusiness {

   @EJB
   private CoursIDao dao;
   
   @EJB
   private MatiereIntervenantIDao matIntDao;
   
   @EJB
   private IntervenantIDao intervDao;

   @Override
   public GenericIDao<Cours> getDao() {
      return dao;
   }

   public void setDao(CoursIDao dao) {
      this.dao = dao;
   }

	@Override
	public List<Cours> getAllPourPromo(int idPromo) {
		return dao.getAllPourPromo(idPromo);
	}
	
	@Override
	public List<Intervenant> getAllIntervenantsAffectables(Cours c) {
		
		List<Intervenant> intervenants = new ArrayList<Intervenant>();
		
		for (Intervenant itv : intervDao.getAll()) {
			
			List<MatiereIntervenant> mis = matIntDao.rechercherParAttributEntier("intervenant.id", itv.getId());
			
			// Check dispo
			if (! intervDao.hasCours(itv.getId(), c.getJour(), c.getBlocHoraire())) {
				
				// Check affectabilité
				for (MatiereIntervenant mi : mis) {
					if (c.getMatiere().getId() == mi.getMatiere().getId()) {
						intervenants.add(itv);
					}
				}
			}
		}
		
		return intervenants;
	}
}
