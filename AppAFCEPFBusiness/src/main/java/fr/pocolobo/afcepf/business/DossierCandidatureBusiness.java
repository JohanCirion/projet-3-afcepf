package fr.pocolobo.afcepf.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.business.api.DossierCandidatureIBusiness;
import fr.pocolobo.afcepf.dao.api.DossierCandidatureIDao;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.entity.DossierCandidature;

@Remote(DossierCandidatureIBusiness.class)
@Stateless
public class DossierCandidatureBusiness extends GenericBusiness<DossierCandidature> 
										implements DossierCandidatureIBusiness{
	@EJB
	private DossierCandidatureIDao dao;
	
	@Override
	public GenericIDao<DossierCandidature> getDao() {
		// TODO Auto-generated method stub
		return dao;
	}

}
