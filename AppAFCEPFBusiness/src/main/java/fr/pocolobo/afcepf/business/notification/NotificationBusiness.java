package fr.pocolobo.afcepf.business.notification;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import fr.pocolobo.afcepf.business.GenericBusiness;
import fr.pocolobo.afcepf.business.api.AdministrationIBusiness;
import fr.pocolobo.afcepf.business.api.notification.NotificationIBusiness;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.dao.api.NotificationIDao;
import fr.pocolobo.afcepf.entity.notification.Notification;
import fr.pocolobo.afcepf.entity.notification.Notification.Type;
import fr.pocolobo.afcepf.entity.utilisateurs.Administration;
import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;

@Remote (NotificationIBusiness.class)
@Stateful
public class NotificationBusiness extends GenericBusiness<Notification> implements NotificationIBusiness {

	@EJB
	private NotificationIDao dao;
	
	@EJB
	private AdministrationIBusiness adminDao;
	
	@Override
	public GenericIDao<Notification> getDao() {
		return dao;
	}

	public void setDao(NotificationIDao dao) {
		this.dao = dao;
	}

	@Override
	public void creerNotification(Utilisateur u, Type t, String message, String url) {
		Notification n = new Notification();
		n.setUtilisateur(u);
		n.setShortMessage(message);
		n.setUrl(url);
		n.setType(t);
		
		dao.ajouter(n);
	}

	@Override
	public void notifierAdministration(String message, Type t, String url) {

		List<Administration> admins = adminDao.getAll();
		
		for (Administration a : admins) {
			creerNotification(a, t, message, url);
		}
		
	}

	@Override
	public List<Notification> getAllPourUtilisateur(Utilisateur u) {

		return rechercherParAttributEntier("utilisateur.id", u.getId());
		
	}
}
