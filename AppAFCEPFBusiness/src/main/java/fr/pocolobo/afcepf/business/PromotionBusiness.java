package fr.pocolobo.afcepf.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import fr.pocolobo.afcepf.business.api.CoursIBusiness;
import fr.pocolobo.afcepf.business.api.CursusIBusiness;
import fr.pocolobo.afcepf.business.api.MatiereIBusiness;
import fr.pocolobo.afcepf.business.api.PromotionIBusiness;
import fr.pocolobo.afcepf.business.api.PromotionStagiaireIBusiness;
import fr.pocolobo.afcepf.business.api.StagiaireIBusiness;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.dao.api.PromotionIDao;
import fr.pocolobo.afcepf.entity.pedago.Cours;
import fr.pocolobo.afcepf.entity.pedago.Cursus;
import fr.pocolobo.afcepf.entity.pedago.Matiere;
import fr.pocolobo.afcepf.entity.pedago.Promotion;
import fr.pocolobo.afcepf.entity.pedago.PromotionStagiaire;
import fr.pocolobo.afcepf.entity.utilisateurs.Intervenant;
import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

@Remote(PromotionIBusiness.class)
@Stateful
public class PromotionBusiness extends GenericBusiness<Promotion> implements PromotionIBusiness {

	@EJB
	private PromotionIDao dao;

	@EJB
	private CoursIBusiness coursBu;

	@EJB
	private PromotionStagiaireIBusiness promoStagiaireBu;

	@EJB
	private StagiaireIBusiness stagiaireBu;

	@EJB
	private CursusIBusiness cursusBu;

	@EJB
	private MatiereIBusiness matiereBu;

	@Override
	public GenericIDao<Promotion> getDao() {
		return dao;
	}

	public void setDao(PromotionIDao dao) {
		this.dao = dao;
	}

	@Override
	public Date getDateDebut(int promoId) {

		List<Cours> cours = coursBu.getAllPourPromo(promoId);

		if (cours.isEmpty())
			return null;

		return cours.get(0).getJour();
	}

	@Override
	public Date getDateFin(int promoId) {

		List<Cours> cours = coursBu.getAllPourPromo(promoId);

		if (cours.isEmpty())
			return null;

		return cours.get(cours.size() - 1).getJour();

	}

	public boolean isPasse(Promotion p) {
		
		if (p == null)
			return false;

		Date dateFin = getDateFin(p.getIdPromotion());

		if (dateFin == null)
			return false;

		if (dateFin.compareTo(new Date()) < 0)
			return true;

		return false;

	}

	public boolean isEnCours(Promotion p) {
		
		if (p == null)
			return false;

		Date dateDebut = getDateDebut(p.getIdPromotion());
		Date dateFin = getDateFin(p.getIdPromotion());

		if (dateDebut == null || dateFin == null)
			return false;

		Date now = new Date();

		return (dateDebut.compareTo(now) < 0) && (dateFin.compareTo(now) > 0);
	}

	public boolean isFuture(Promotion p) {
		
		if (p == null)
			return false;

		Date dateDebut = getDateDebut(p.getIdPromotion());

		if (dateDebut == null)
			return false;

		if (dateDebut.compareTo(new Date()) > 0)
			return true;

		return false;
	}

	@Override
	public List<Stagiaire> getAllStagiaires(int promoId) {

		List<PromotionStagiaire> allPs = promoStagiaireBu.rechercherParAttributEntier("promotion.id", promoId);
		List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();

		for (PromotionStagiaire ps : allPs) {
			stagiaires.add(ps.getStagiaire());
		}

		return stagiaires;

	}

	public PromotionStagiaireIBusiness getPromoStagiaireBu() {
		return promoStagiaireBu;
	}

	public void setPromoStagiaireBu(PromotionStagiaireIBusiness promoStagiaireBu) {
		this.promoStagiaireBu = promoStagiaireBu;
	}

	@Override
	public Promotion creerPromotion(Cursus cursusDeploy, Date dateDeploy, int numeroPromo) {

		Promotion p = new Promotion();
		p.setCursus(cursusDeploy);
		p.setNumero(numeroPromo);

		p = ajouter(p);

		creerCours(dateDeploy, cursusDeploy, p);

		return p;
	}
	
	private void creerCours(Date dateDebut, Cursus cursus, Promotion promo) {
		List<Matiere> matieres = matiereBu.getAllPourCursus(cursus.getId());

		Date courante = dateDebut;
		int blocCourant = 0;

		for (Matiere mat : matieres) {

			for (int i = 0; i < mat.getDureeDemiJournees(); ++i) {

				Cours c = new Cours();
				c.setBlocHoraire(blocCourant);
				c.setJour(courante);

				blocCourant++;

				// Incrémentation du jour
				if (blocCourant >= 2) {
					blocCourant %= 2;

					courante = nextValidDay(courante);
				}
				
				c.setBlocHoraire(blocCourant);
				c.setDureeBlocs(1);
				
				c.setPromotion(promo);
				c.setMatiere(mat);
				
				// Affectation intervenant
				affecterIntervenant(c);
				
				coursBu.ajouter(c);
			}
		}
	}
	
	private void affecterIntervenant(Cours c) {
		List<Intervenant> itvAffectables = coursBu.getAllIntervenantsAffectables(c);
		
		if (! itvAffectables.isEmpty())
			c.setIntervenant(itvAffectables.get(0));
	}
	
	private Date nextValidDay(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		
		do {
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1);
		} while (! isOpen(cal.getTime()));
		
		return cal.getTime();
	}
	
	private boolean isOpen(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		
		return cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
				&& cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY;
	}

	@Override
	public List<Promotion> getAllPourCursus(int id) {

		return rechercherParAttributEntier("cursus.id", id);
	}

}
