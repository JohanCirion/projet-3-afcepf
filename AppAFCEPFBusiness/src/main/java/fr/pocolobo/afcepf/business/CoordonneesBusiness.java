package fr.pocolobo.afcepf.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.business.api.CoordonneesIBusiness;
import fr.pocolobo.afcepf.dao.api.CoordonneesIDao;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.entity.Coordonnees;

@Remote(CoordonneesIBusiness.class)
@Stateless
public class CoordonneesBusiness extends GenericBusiness<Coordonnees> 
								implements CoordonneesIBusiness{
	@EJB
	private CoordonneesIDao dao;
	
	@Override
	public GenericIDao<Coordonnees> getDao() {
		// TODO Auto-generated method stub
		return dao;
	}

}
