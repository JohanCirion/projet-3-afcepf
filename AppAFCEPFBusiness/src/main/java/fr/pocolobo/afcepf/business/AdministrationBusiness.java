package fr.pocolobo.afcepf.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import fr.pocolobo.afcepf.business.api.AdministrationIBusiness;
import fr.pocolobo.afcepf.dao.api.AdministrationIDao;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.entity.utilisateurs.Administration;

@Remote (AdministrationIBusiness.class)
@Stateful
public class AdministrationBusiness extends GenericBusiness<Administration> implements AdministrationIBusiness {

   @EJB
   private AdministrationIDao dao;

   @Override
   public GenericIDao<Administration> getDao() {
      return dao;
   }

   public void setDao(AdministrationIDao dao) {
      this.dao = dao;
   }
}
