package fr.pocolobo.afcepf.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.business.api.UtilisateurIBusiness;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.dao.api.UtilisateurIDao;
import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;

@Remote (UtilisateurIBusiness.class)
@Stateless
public class UtilisateurBusiness extends GenericBusiness<Utilisateur> implements UtilisateurIBusiness {

   @EJB
   private UtilisateurIDao dao;

   @Override
   public GenericIDao<Utilisateur> getDao() {
      return dao;
   }

   public void setDao(UtilisateurIDao dao) {
      this.dao = dao;
   }
   
	@Override
	public Utilisateur creerUtilisateur(Utilisateur utilisateur) {
		 Utilisateur utilisateurReturned = null;
	        if(!dao.exists(utilisateur.getLogin())) {
	            utilisateurReturned = dao.ajouter(utilisateur);
	        }

	        return utilisateurReturned;
	}

	@Override
	public Utilisateur connecter(String login, String password) {
		 Utilisateur utilisateurConnecte = null;
		 utilisateurConnecte = dao.authenticate(login, password);
		
		return utilisateurConnecte;
	}
	
	@Override
	public Utilisateur attribuerIdentifiant(Utilisateur utilisateur) {
		
		
		utilisateur.setLogin(dao.generateLogin(utilisateur));
		utilisateur.setPassword(dao.generatePasswordCrypte(utilisateur));
		
		
		return utilisateur;
		
	}

	@Override
	public String genererLogin(Utilisateur utilisateur) {
		
		utilisateur.setLogin(dao.generateLogin(utilisateur));
		
		//return utilisateur.getLogin();
		
		return dao.generateLogin(utilisateur);
	}
	
	@Override
	public String genererPassword(Utilisateur utilisateur) {
		
		utilisateur.setPassword(dao.generatePasswordCrypte(utilisateur));
		
		//return utilisateur.getLogin();
		
		return dao.generatePasswordCrypte(utilisateur);
	}
}
