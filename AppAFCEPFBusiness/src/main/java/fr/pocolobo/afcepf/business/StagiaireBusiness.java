package fr.pocolobo.afcepf.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.business.api.DossierCandidatureIBusiness;
import fr.pocolobo.afcepf.business.api.PromotionIBusiness;
import fr.pocolobo.afcepf.business.api.StagiaireIBusiness;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.dao.api.PromotionStagiaireIDao;
import fr.pocolobo.afcepf.dao.api.StagiaireIDao;
import fr.pocolobo.afcepf.entity.DossierCandidature;
import fr.pocolobo.afcepf.entity.pedago.Promotion;
import fr.pocolobo.afcepf.entity.pedago.PromotionStagiaire;
import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

@Remote(StagiaireIBusiness.class)
@Stateless
public class StagiaireBusiness extends GenericBusiness<Stagiaire> implements StagiaireIBusiness {

	@EJB
	StagiaireIDao dao;
	
	@EJB
	PromotionIBusiness promoBu;
	
	@EJB
	PromotionStagiaireIDao psDao;
	
	@EJB
	DossierCandidatureIBusiness dcBu;
	
	@Override
	public GenericIDao<Stagiaire> getDao() {
		return dao;
	}

	@Override
	public List<Promotion> getPromotions(int stagiaireId) {
		List<PromotionStagiaire> allPS = psDao.rechercherParAttributEntier("stagiaire.id", stagiaireId);
		
		List<Promotion> r = new ArrayList<Promotion>();
		
		for (PromotionStagiaire ps : allPS) {
			r.add(promoBu.rechercherParId(ps.getPromotion().getIdPromotion()));
		}
		
		return r;
	}

	@Override
	public List<DossierCandidature> getDossiersCandidature(int stagiaireId) {
		List<DossierCandidature> r = dcBu.rechercherParAttributEntier("stagiaire.id", stagiaireId);
		
		return r;
	}
	
	@Override
	public Promotion getDernierePromotion(int stagiaireId) {
		
		List<Promotion> liste = getPromotions(stagiaireId);
		
		Promotion r = null;
		Date now = new Date();
		
		for (Promotion p : liste) {

			if (p == null)
				continue;
			
			Date parcouru = promoBu.getDateDebut(p.getIdPromotion());
			
			if (parcouru.compareTo(now) > 0)	// Date future
				continue;
			
			if (r == null) {					// Première date retenue
				r = p;
				continue;
			}
			
			Date retenu = promoBu.getDateDebut(r.getIdPromotion());
			
			if (retenu.compareTo(parcouru) > 0) {
				r = p;
			}
		}
		
		return r;
	}
	
	@Override
	public String getStatutStagiaire(Stagiaire stagiaire) {

		if (isCandidatInitial(stagiaire)) 	return "Candidat initial";
		if (isEnAttenteEI(stagiaire)) 		return "En attente EI";
		if (isEnAttenteRI(stagiaire)) 		return "En attente RI";
		if (isCandidat(stagiaire)) 			return "Candidat";
		if (isStagiaireActif(stagiaire)) 	return "Stagiaire actif";
		if (isStagiaireFutur(stagiaire)) 	return "Stagiaire futur";
		if (isStagiairePasse(stagiaire)) 	return "Stagiaire passé";
		if (isCandidatRefuse(stagiaire)) 	return "Candidat refusé";

		return "";
		
	}

	@Override
	public List<DossierCandidature> getCandidaturesEnCours(Stagiaire stagiaire) {
		
		List<DossierCandidature> dossiers = getDossiersCandidature(stagiaire.getId());
		
		List<DossierCandidature> r = new ArrayList<>();
		
		for (DossierCandidature dc : dossiers) {
			if (! dc.isRefuse() && ! dc.isAccepte())
				r.add(dc);
		}
		
		return r;
	}
	
	@Override
	public boolean isCandidatInitial(Stagiaire stagiaire) {
		
		for (DossierCandidature dc : getCandidaturesEnCours(stagiaire)) {
			if (dc.getAvancement() == DossierCandidature.Avancement.initial)
				return true;
		}
		
		return false;

	}
	
	@Override
	public boolean isCandidatRefuse(Stagiaire stagiaire) {

		for (DossierCandidature dc : getDossiersCandidature(stagiaire.getId())) {
			if (dc.isRefuse())
				return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isEnAttenteRI(Stagiaire stagiaire) {
		
		for (DossierCandidature dc : getCandidaturesEnCours(stagiaire)) {
			if (dc.getAvancement() == DossierCandidature.Avancement.attenteRI)
				return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isCandidat(Stagiaire stagiaire) {

		return ! getCandidaturesEnCours(stagiaire).isEmpty();
	}
	
	@Override
	public boolean isEnAttenteEI(Stagiaire stagiaire) {

		for (DossierCandidature dc : getCandidaturesEnCours(stagiaire)) {
			if (dc.getAvancement() == DossierCandidature.Avancement.attenteEI)
				return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isStagiaireFutur(Stagiaire stagiaire) {

		for (Promotion p : getPromotions(stagiaire.getId())) {
			if (promoBu.isFuture(p))
				return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isStagiaireActif(Stagiaire stagiaire) {

		for (Promotion p : getPromotions(stagiaire.getId())) {
			if (promoBu.isEnCours(p))
				return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isStagiairePasse(Stagiaire stagiaire) {

		for (Promotion p : getPromotions(stagiaire.getId())) {
			if (promoBu.isPasse(p))
				return true;
		}
		
		return false;
	}
}
