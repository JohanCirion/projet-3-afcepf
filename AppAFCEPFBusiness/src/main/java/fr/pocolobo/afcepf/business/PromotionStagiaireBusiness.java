package fr.pocolobo.afcepf.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import fr.pocolobo.afcepf.business.api.PromotionStagiaireIBusiness;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.dao.api.PromotionStagiaireIDao;
import fr.pocolobo.afcepf.entity.pedago.PromotionStagiaire;

@Remote (PromotionStagiaireIBusiness.class)
@Stateful
public class PromotionStagiaireBusiness extends GenericBusiness<PromotionStagiaire> implements PromotionStagiaireIBusiness {

   @EJB
   private PromotionStagiaireIDao dao;

   @Override
   public GenericIDao<PromotionStagiaire> getDao() {
      return dao;
   }

   public void setDao(PromotionStagiaireIDao dao) {
      this.dao = dao;
   }
}
