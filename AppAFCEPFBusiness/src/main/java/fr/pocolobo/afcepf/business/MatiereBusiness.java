package fr.pocolobo.afcepf.business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.business.api.MatiereIBusiness;
import fr.pocolobo.afcepf.dao.api.GenericIDao;
import fr.pocolobo.afcepf.dao.api.MatiereIDao;
import fr.pocolobo.afcepf.dao.api.ModuleIDao;
import fr.pocolobo.afcepf.entity.pedago.Matiere;
import fr.pocolobo.afcepf.entity.pedago.MatiereModule;
import fr.pocolobo.afcepf.entity.pedago.Module;

@Remote (MatiereIBusiness.class)
@Stateless
public class MatiereBusiness extends GenericBusiness<Matiere> implements MatiereIBusiness {

   @EJB
   private MatiereIDao dao;
   
   @EJB
   private ModuleIDao moduleDao;

   @Override
   public GenericIDao<Matiere> getDao() {
      return dao;
   }

   public void setDao(MatiereIDao dao) {
      this.dao = dao;
   }

	@Override
	public List<Matiere> getAllPourCursus(int cursusId) {
		
		List<Module> modules = moduleDao.getAllForCursus(cursusId);
		List<Matiere> matieres = new ArrayList<>();
		
		for (Module m : modules) {
			for (MatiereModule mm : m.getMatiereModules()) {
				matieres.add(mm.getMatiere());
			}
		}
		
		return matieres;
	}
}
