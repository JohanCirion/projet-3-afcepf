package fr.pocolobo.afcepf.business.api;

import java.util.List;

public interface GenericIBusiness<T> {
    T ajouter(T t);
    boolean supprimer (T t);
    T modifier(T t);
    T rechercherParId (Integer i);
    List<T> getAll();

    List<T> rechercherParAttributContenu (String nomAttribut, String valeur);
	public List<T> rechercherParAttributEntier (String nomAttribut, int valeur);
}
