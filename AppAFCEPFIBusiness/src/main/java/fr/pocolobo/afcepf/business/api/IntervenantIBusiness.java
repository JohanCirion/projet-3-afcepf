package fr.pocolobo.afcepf.business.api;

import fr.pocolobo.afcepf.entity.utilisateurs.Intervenant;

public interface IntervenantIBusiness extends GenericIBusiness<Intervenant> {
}
