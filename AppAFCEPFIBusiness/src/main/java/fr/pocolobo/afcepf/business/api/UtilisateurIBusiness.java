package fr.pocolobo.afcepf.business.api;

import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;

public interface UtilisateurIBusiness extends GenericIBusiness<Utilisateur> {
	
	public String genererLogin(Utilisateur utilisateur);
	
	public Utilisateur creerUtilisateur(Utilisateur utilisateur);
	
	public Utilisateur connecter(String login, String password);
	
	public Utilisateur attribuerIdentifiant(Utilisateur utilisateur);
	
	public String genererPassword(Utilisateur utilisateur); 
}
