//package fr.pocolobo.afcepf.business.api;
//
//import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;
//
//public interface ConnexionIBusiness extends GenericIBusiness<Utilisateur>{
//	
//	Utilisateur creerUtilisateur (Utilisateur utilisateur);
//	
//	Utilisateur connecter (String login, String password);
//	
//	Utilisateur attribuerIdentifiant(Utilisateur utilisateur);
//	
//	String genererLogin(Utilisateur utilisateur);
//	
//	String genererMdpCrypte(Utilisateur utilisateur);
//		
//	String decrypterMdp (String password);
//	
//}
