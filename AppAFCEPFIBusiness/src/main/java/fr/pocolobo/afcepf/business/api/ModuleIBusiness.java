package fr.pocolobo.afcepf.business.api;

import java.util.List;

import fr.pocolobo.afcepf.entity.pedago.Module;

public interface ModuleIBusiness extends GenericIBusiness<Module> {
	public List<Module> getAllForCursus(int idCursus);
}
