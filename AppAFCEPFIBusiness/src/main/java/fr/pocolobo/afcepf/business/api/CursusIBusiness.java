package fr.pocolobo.afcepf.business.api;

import fr.pocolobo.afcepf.business.api.GenericIBusiness;
import fr.pocolobo.afcepf.entity.pedago.Cursus;

public interface CursusIBusiness extends GenericIBusiness<Cursus> {
}
