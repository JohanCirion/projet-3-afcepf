package fr.pocolobo.afcepf.business.api;

import java.util.List;

import fr.pocolobo.afcepf.entity.pedago.Matiere;

public interface MatiereIBusiness extends GenericIBusiness<Matiere> {
	
	List<Matiere> getAllPourCursus(int cursusId);
	
}
