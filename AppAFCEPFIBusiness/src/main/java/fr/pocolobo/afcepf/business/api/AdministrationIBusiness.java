package fr.pocolobo.afcepf.business.api;

import fr.pocolobo.afcepf.entity.utilisateurs.Administration;

public interface AdministrationIBusiness extends GenericIBusiness<Administration> {
}
