package fr.pocolobo.afcepf.business.api;

import fr.pocolobo.afcepf.entity.DossierCandidature;

public interface DossierCandidatureIBusiness extends GenericIBusiness<DossierCandidature>{

}
