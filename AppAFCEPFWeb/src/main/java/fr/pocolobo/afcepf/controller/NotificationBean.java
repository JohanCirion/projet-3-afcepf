package fr.pocolobo.afcepf.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import fr.pocolobo.afcepf.business.api.notification.NotificationIBusiness;
import fr.pocolobo.afcepf.entity.notification.Notification;

@ManagedBean
@SessionScoped
public class NotificationBean implements Serializable {

	//	Attributs
	private static final long serialVersionUID = 1L;

	@EJB
	private NotificationIBusiness notificationBu;

	private List<Notification> notifications;
	
	private String numNotifications = "";
	
	private String notificationDotDisplay = "none";

    @ManagedProperty(value="#{mbConnect}")
    private ConnexionBean mbConnect;
	
	//	Initialisation
	@PostConstruct
	public void init() {
		pollNotifications();
	}
	
	// Méthodes
	
	public String getNotificationMessage() {

		return "Alertes" + (numNotifications.isEmpty() ? "" : " (" + numNotifications + ")");
	}

	public String getNotificationDotDisplay() {
		return notificationDotDisplay;
	}

	public void setNotificationDotDisplay(String notificationDotDisplay) {
		this.notificationDotDisplay = notificationDotDisplay;
	}

	public void pollNotifications() {
		
		if (mbConnect.getUtilisateur() != null) {
			notifications = notificationBu.getAllPourUtilisateur(mbConnect.getUtilisateur());
			
			final int num = notifications.size();
			numNotifications = num <= 0 ? "" : (num > 9 ? "9+" : "" + num);
			notificationDotDisplay = num > 0 ? "block" : "none";
			
			return;
		}
		
		numNotifications = "";
		notificationDotDisplay = "none";
	}

	// Getters/setters
	
	public NotificationIBusiness getNotificationBu() {
		return notificationBu;
	}

	public void setNotificationBu(NotificationIBusiness notificationBu) {
		this.notificationBu = notificationBu;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public String getNumNotifications() {
		return numNotifications;
	}

	public void setNumNotifications(String numNotifications) {
		this.numNotifications = numNotifications;
	}

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}
	
	
}