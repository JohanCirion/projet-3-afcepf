package fr.pocolobo.afcepf.controller;

import java.util.List;

import fr.pocolobo.afcepf.business.api.GenericIBusiness;

public abstract class GenericRechercheBean<T> {

	private String champs;
	private List<T> resultats;
	
	protected abstract String getRechercheAttribut();
	
	protected abstract GenericIBusiness<T> getBusiness();
	
	public String getChamps() {
		return champs;
	}

	public void setChamps(String champs) {
		this.champs = champs;
	}

	public List<T> getResultats() {
		return resultats;
	}

	public void setResultats(List<T> resultats) {
		this.resultats = resultats;
	}

	public String rechercher() {
		resultats = getBusiness().rechercherParAttributContenu(getRechercheAttribut(), champs);
		return "testRecherche.xhtml?use-redirect=true";
	}
}
