package fr.pocolobo.afcepf.controller;

import java.util.ArrayList;
import java.util.List;

import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;
import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur.ProfileType;

public class AccessManager {
	
	public static class Profile {

		List<Integer> authorizedUserIds;
		List<ProfileType> authorizedProfileTypes;
		
		public Profile(List<Integer> ids, List<ProfileType> types) {
			authorizedUserIds = ids;
			authorizedProfileTypes = types;
		}
		
		public static Profile avecId(int idUtilisateur) {
			
			List<Integer> ids = new ArrayList<Integer>();
			ids.add(idUtilisateur);
			
			return new Profile(ids, null);
		}
		
		public static Profile administration() {
			
			List<ProfileType> t = new ArrayList<ProfileType>();
			t.add(ProfileType.administrateur);
			
			return new Profile(null, t);
		}
	}
	
	public static boolean canAccess(Utilisateur utilisateur, Profile profile) {

		if (utilisateur == null) {
			return false;
		}
		
		if (profile.authorizedUserIds != null && profile.authorizedUserIds.contains(utilisateur.getId())) {
			return true;
		}
		
		if (profile.authorizedProfileTypes != null && profile.authorizedProfileTypes.contains(utilisateur.getProfile())) {
			return true;
		}
		
		return false;
	}
	
}
