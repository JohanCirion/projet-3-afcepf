package fr.pocolobo.afcepf.controller.rh;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.pocolobo.afcepf.business.api.StagiaireIBusiness;
import fr.pocolobo.afcepf.controller.AccessManager;
import fr.pocolobo.afcepf.controller.ConnexionBean;
import fr.pocolobo.afcepf.controller.NavigationBean;
import fr.pocolobo.afcepf.entity.pedago.Promotion;
import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

@ManagedBean
@ViewScoped
public class ListeStagiairesBean {

	private List<Stagiaire> listeStagiaires;

	@EJB
	private StagiaireIBusiness stagBu;

	@ManagedProperty(value = "#{mbConnect}")
	private ConnexionBean mbConnect;

	@PostConstruct
	public void init() {
		listeStagiaires = stagBu.getAll();
	}

	public String getNomDernierePromo(Stagiaire stag) {
		
		Promotion p = stagBu.getDernierePromotion(stag.getId());
		
		if (p == null)
			return "";
		
		String codeCursus = p.getCursus().getCode();
		return codeCursus + p.getNumero();
	}
	
	public String getStatut(Stagiaire stag) {
		return stagBu.getStatutStagiaire(stag);
	}
	
	// =====================================================
	// Restriction d'accès

	public String checkAccess() {

		AccessManager.Profile p = AccessManager.Profile.administration();
		if (!AccessManager.canAccess(mbConnect.getUtilisateur(), p)) {
			return NavigationBean.forbiddenUrl;
		}

		return null;
	}

	// =====================================================
	// Recherche

	private String champs;

	public String getChamps() {
		return champs;
	}

	public void setChamps(String champs) {
		this.champs = champs;
	}

	public void rechercher() {
		if (champs == null) {
			setListeStagiaires(stagBu.getAll());
			return;
		}

		listeStagiaires = stagBu.rechercherParAttributContenu("coordonnees.nom coordonnees.prenom", champs);
	}

	// =====================================================
	// Getters/setters

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}

	public List<Stagiaire> getListeStagiaires() {
		return listeStagiaires;
	}

	public void setListeStagiaires(List<Stagiaire> listeStagiaires) {
		this.listeStagiaires = listeStagiaires;
	}
	
}