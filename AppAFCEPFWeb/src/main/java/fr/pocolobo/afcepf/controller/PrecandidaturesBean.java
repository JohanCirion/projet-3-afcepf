package fr.pocolobo.afcepf.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.pocolobo.afcepf.business.api.CoordonneesIBusiness;
import fr.pocolobo.afcepf.business.api.CursusIBusiness;
import fr.pocolobo.afcepf.business.api.DossierCandidatureIBusiness;
import fr.pocolobo.afcepf.business.api.StagiaireIBusiness;
import fr.pocolobo.afcepf.business.api.UtilisateurIBusiness;
import fr.pocolobo.afcepf.business.api.notification.NotificationIBusiness;
import fr.pocolobo.afcepf.entity.Adresse;
import fr.pocolobo.afcepf.entity.Coordonnees;
import fr.pocolobo.afcepf.entity.DossierCandidature;
import fr.pocolobo.afcepf.entity.DossierCandidature.Avancement;
import fr.pocolobo.afcepf.entity.pedago.Cursus;
import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

@ManagedBean(name="mbCandidatBean")
@ViewScoped
public class PrecandidaturesBean {

	private String messageErreur;
	private Stagiaire candidat = new Stagiaire();
	private Coordonnees coordonnees = new Coordonnees();
	private DossierCandidature preCandidature = new DossierCandidature();
	private Cursus cursusChoisi = new Cursus();
	
	private List<Cursus> listeCursus;
	private boolean showInvader = false;

	@EJB
	DossierCandidatureIBusiness ibuCandidature;
	@EJB
	CursusIBusiness ibuCursus;
	@EJB
	StagiaireIBusiness ibuStagiaire;
	@EJB
	CoordonneesIBusiness ibuCoord;
	@EJB
	UtilisateurIBusiness ibuUtilisateur;
	@EJB
	NotificationIBusiness ibuNotification;
	
	@ManagedProperty(value = "#{alertManagerBean}")
	AlertManagerBean alertManager;
	
	@PostConstruct
	void init() {
		reset();
	}
	
	private void reset() {
		
		candidat = new Stagiaire();
		coordonnees = new Coordonnees();
		preCandidature = new DossierCandidature();
		cursusChoisi = new Cursus();
		
		showInvader = false;
		messageErreur = "";
		
		candidat.setCoordonnees(coordonnees);
		preCandidature.setStagiaire(candidat);
		listeCursus = ibuCursus.getAll();
	}
	
	//Méthode pour le bouton valider pour persister toutes les données.
	public String ajouter() {
		
		messageErreur = "";
		
		if (cursusChoisi == null || cursusChoisi == new Cursus()) {
			messageErreur = "Veuillez choisir un cursus.";
			return null;
		}
		
		if (coordonnees == null
			|| coordonnees.getNom() == null
			|| coordonnees.getNom().isEmpty()
			|| coordonnees.getPrenom() == null
			|| coordonnees.getPrenom().isEmpty()
			|| coordonnees.getEmail() == null
			|| coordonnees.getEmail().isEmpty()
			|| cursusChoisi.getNom() == null
			|| cursusChoisi.getNom().isEmpty()) {
			messageErreur = "Veuillez renseigner tous les champs.";
			return null;
		}

		coordonnees.setAdresse(new Adresse());
		coordonnees.setPhoto("images/photosUtilisateurs/default.jpg");
        coordonnees = ibuCoord.ajouter(coordonnees);
        
        candidat.setCoordonnees(coordonnees);
        candidat = (Stagiaire) ibuUtilisateur.attribuerIdentifiant(candidat);
        candidat.setCoordonnees(coordonnees);
        candidat = ibuStagiaire.ajouter(candidat);
        
        preCandidature.setStagiaire(candidat);
        preCandidature.setCursusDemande(cursusChoisi);
        preCandidature.setAvancement(Avancement.initial);
        preCandidature.setDateDepot(new Date());
        preCandidature = ibuCandidature.ajouter(preCandidature);
        
        showInvader = true;
        
        // Notifier l'administration
        //String redirection = "listeCandidatures";
        //ibuNotification.notifierAdministration("Une nouvelle candidature a été déposée pour " + cursusChoisi.getNom() + ".", redirection);
    
        alertManager.refresh();
        
        return null;
    }

	public String choixCursus(Cursus cursus) {
		cursusChoisi = cursus;		
		return null; //"precandidature?use-redirect=true";
	}
	
	
	public PrecandidaturesBean() {
		super();
	}

	public Stagiaire getCandidat() {
		return candidat;
	}

	public void setCandidat(Stagiaire candidat) {
		this.candidat = candidat;
	}

	public Coordonnees getCoordonnees() {
		return coordonnees;
	}

	public void setCoordonnees(Coordonnees coordonnees) {
		this.coordonnees = coordonnees;
	}

	public DossierCandidature getPreCandidature() {
		return preCandidature;
	}

	public void setPreCandidature(DossierCandidature preCandidature) {
		this.preCandidature = preCandidature;
	}

	public DossierCandidatureIBusiness getIbuCandidature() {
		return ibuCandidature;
	}

	public void setIbuCandidature(DossierCandidatureIBusiness ibuCandidature) {
		this.ibuCandidature = ibuCandidature;
	}

	public CursusIBusiness getIbuCursus() {
		return ibuCursus;
	}

	public void setIbuCursus(CursusIBusiness ibuCursus) {
		this.ibuCursus = ibuCursus;
	}

	public StagiaireIBusiness getIbuStagiaire() {
		return ibuStagiaire;
	}

	public void setIbuStagiaire(StagiaireIBusiness ibuStagiaire) {
		this.ibuStagiaire = ibuStagiaire;
	}

	public DossierCandidatureIBusiness getIbu() {
		return ibuCandidature;
	}


	public void setIbu(DossierCandidatureIBusiness ibu) {
		this.ibuCandidature = ibu;
	}


	public Cursus getCursusChoisi() {
		return cursusChoisi;
	}


	public void setCursusChoisi(Cursus cursus) {
		this.cursusChoisi = cursus;
	}


	public List<Cursus> getListeCursus() {
		return listeCursus;
	}


	public void setListeCursus(List<Cursus> listeCursus) {
		this.listeCursus = listeCursus;
	}
	
	public boolean isShowInvader() {
		return showInvader;
	}

	public void setShowInvader(boolean showInvader) {
		this.showInvader = showInvader;
	}

	public String getMessageErreur() {
		return messageErreur;
	}

	public void setMessageErreur(String messageErreur) {
		this.messageErreur = messageErreur;
	}

	public CoordonneesIBusiness getIbuCoord() {
		return ibuCoord;
	}

	public void setIbuCoord(CoordonneesIBusiness ibuCoord) {
		this.ibuCoord = ibuCoord;
	}

	public UtilisateurIBusiness getIbuUtilisateur() {
		return ibuUtilisateur;
	}

	public void setIbuUtilisateur(UtilisateurIBusiness ibuUtilisateur) {
		this.ibuUtilisateur = ibuUtilisateur;
	}

	public NotificationIBusiness getIbuNotification() {
		return ibuNotification;
	}

	public void setIbuNotification(NotificationIBusiness ibuNotification) {
		this.ibuNotification = ibuNotification;
	}

	public AlertManagerBean getAlertManager() {
		return alertManager;
	}

	public void setAlertManager(AlertManagerBean alertManager) {
		this.alertManager = alertManager;
	}
	
	
}
