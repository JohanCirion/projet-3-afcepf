package fr.pocolobo.afcepf.controller.rh;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;


import fr.pocolobo.afcepf.business.api.CiviliteIBusiness;
import fr.pocolobo.afcepf.business.api.CoordonneesIBusiness;
import fr.pocolobo.afcepf.business.api.UtilisateurIBusiness;
import fr.pocolobo.afcepf.controller.AccessManager;
import fr.pocolobo.afcepf.controller.ConnexionBean;
import fr.pocolobo.afcepf.controller.NavigationBean;
import fr.pocolobo.afcepf.entity.Adresse;
import fr.pocolobo.afcepf.entity.Civilite;
import fr.pocolobo.afcepf.entity.Coordonnees;
import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;
import fr.pocolobo.afcepf.entity.utilisateurs.UtilisateurHelper;

@ManagedBean(name = "mbEditionUtilisateur")
@SessionScoped
public class EditionUtilisateurBean implements Serializable {

	private static final String thisPage = "editionUtilisateur?faces-redirect=true";

	private static final long serialVersionUID = 1L;

	private Utilisateur utilisateurEdite;
	private List<Civilite> civilites;
	private Coordonnees coordonnees;
	private String message;

	private String nouveauLogin;

	private String nouveauMdp;

	@EJB
	private UtilisateurIBusiness utilisateurBu;

	@EJB
	private CoordonneesIBusiness coordonneesBu;

	@EJB
	private CiviliteIBusiness ibuCivil;

	@ManagedProperty(value="#{mbConnect}")
	private ConnexionBean mbConnect;

	@PostConstruct
	public void init() {

		civilites = ibuCivil.getAll();
	}

	public String checkAccess() {

		AccessManager.Profile p = AccessManager.Profile.administration();		
		if (! AccessManager.canAccess(mbConnect.getUtilisateur(), p)) {
			return NavigationBean.forbiddenUrl;
		}

		return null;
	}

	private void copierCoordonnees() {

		Coordonnees orig = utilisateurEdite.getCoordonnees();
		coordonnees = coordonneesBu.rechercherParId(orig.getId());


		// Rustine pour éviter de travailler avec une adresse nulle
		if (coordonnees.getAdresse() == null) {
			coordonnees.setAdresse(new Adresse());
		}
	}


	public String enregistrer() {


		coordonneesBu.modifier(utilisateurEdite.getCoordonnees());


		return thisPage;

	}

	public void annuler() {
		message ="";			
		copierCoordonnees();


	}


	public String afficher(Utilisateur u) {

		utilisateurEdite = u;

		return thisPage;
	}


	public String utilisateurType () {

		if (UtilisateurHelper.isStagiaire(utilisateurEdite))				return "Stagiaire"; 
		if (UtilisateurHelper.isIntervenant(utilisateurEdite))	return "Intervenant"; 
		if (UtilisateurHelper.isAdministration(utilisateurEdite))				return "Administration"; 

		return "";
	}

	public String genererLogin() {

		nouveauLogin =	utilisateurBu.genererLogin(utilisateurEdite);

		utilisateurEdite.setLogin(nouveauLogin);
		utilisateurEdite = utilisateurBu.modifier(utilisateurEdite);
		
		return thisPage;

	}


	public String genererMdp() {

		nouveauMdp = utilisateurBu.genererPassword(utilisateurEdite);

		utilisateurEdite.setPassword(nouveauMdp);
		utilisateurEdite = utilisateurBu.modifier(utilisateurEdite);
	
		return thisPage;

	}

	// Getter et setter :

	public List<Civilite> getCivilites() {
		return civilites;
	}

	public void setCivilites(List<Civilite> civilites) {
		this.civilites = civilites;
	}

	public Coordonnees getCoordonnees() {
		return coordonnees;
	}

	public void setCoordonnees(Coordonnees coordonnees) {
		this.coordonnees = coordonnees;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public CiviliteIBusiness getIbuCivil() {
		return ibuCivil;
	}

	public void setIbuCivil(CiviliteIBusiness ibuCivil) {
		this.ibuCivil = ibuCivil;
	}

	public Utilisateur getUtilisateurEdite() {
		return utilisateurEdite;
	}

	public void setUtilisateurEdite(Utilisateur utilisateurEdite) {
		this.utilisateurEdite = utilisateurEdite;
	}

	public UtilisateurIBusiness getUtilisateurBu() {
		return utilisateurBu;
	}

	public void setUtilisateurBu(UtilisateurIBusiness utilisateurBu) {
		this.utilisateurBu = utilisateurBu;
	}

	public CoordonneesIBusiness getCoordonneesBu() {
		return coordonneesBu;
	}

	public void setCoordonneesBu(CoordonneesIBusiness coordonneesBu) {
		this.coordonneesBu = coordonneesBu;
	}

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}

	public static String getThispage() {
		return thisPage;
	}

	public String getNouveauLogin() {
		return nouveauLogin;
	}

	public void setNouveauLogin(String nouveauLogin) {
		this.nouveauLogin = nouveauLogin;
	}

	public String getNouveauMdp() {
		return nouveauMdp;
	}

	public void setNouveauMdp(String nouveauMdp) {
		this.nouveauMdp = nouveauMdp;
	}






}
