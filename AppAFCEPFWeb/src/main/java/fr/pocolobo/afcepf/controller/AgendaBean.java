package fr.pocolobo.afcepf.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

import fr.pocolobo.afcepf.business.api.CoursIBusiness;
import fr.pocolobo.afcepf.business.api.StagiaireIBusiness;
import fr.pocolobo.afcepf.entity.pedago.Cours;
import fr.pocolobo.afcepf.entity.pedago.Promotion;

@ManagedBean
@ViewScoped
public class AgendaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private ScheduleModel eventModel;

    @ManagedProperty(value="#{mbConnect}")
    private ConnexionBean mbConnect;
    
    @EJB
    private StagiaireIBusiness stagiaireBu;
    
    @EJB
    private CoursIBusiness coursBu;
    
	@PostConstruct
	public void init() {
		eventModel = new DefaultScheduleModel();
		
		if (mbConnect.isStagiaire()) {
			System.out.println("Load stagiaire");
			loadStagiaire();
		}
	}
	
	//=========================================================================
	// Accès restreint (utilisateur identifié)
	
	public String checkAccess() {
	
		if (! mbConnect.isConnecte()) {
			return NavigationBean.forbiddenUrl;
		}
		
		return null;
	}

	//=========================================================================
	// Getters/Setters
	
	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}
	
	//=========================================================================
	
	private void loadStagiaire() {
		List<Promotion> promos = stagiaireBu.getPromotions(mbConnect.getUtilisateur().getId());
		List<Cours> cours = new ArrayList<Cours>();
		
		for (Promotion p : promos) {
			List<Cours> coursPromo = coursBu.getAllPourPromo(p.getIdPromotion());
			
			for (Cours c : coursPromo) {
				cours.add(c);
			}
		}
		
		for (Cours c : cours) {
			addCours(c);
		}
	}
	
	private void addCours(Cours c) {
		
		String intervenant = c.getIntervenant() == null ? "Non encadré" : c.getIntervenant().getCoordonnees().getNom();
		String msg = c.getMatiere().getNom() + " (" + intervenant + ")";

		for (int i = 0; i < c.getDureeBlocs(); ++i) {
			addDemiJournee(msg, c.getJour(), c.getBlocHoraire() + i == 1);
		}
	}
	
	private void addDemiJournee(String msg, Date jour, boolean apresMidi) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(jour);
        cal.set(Calendar.HOUR_OF_DAY, apresMidi ? 14 : 0);
        cal.set(Calendar.MINUTE, 0);
		
        Date debut = cal.getTime();
		
        cal.set(Calendar.HOUR_OF_DAY, apresMidi ? 17 : 12);
        cal.set(Calendar.MINUTE, 30);
        
		Date fin = cal.getTime(); 
		
		addEvent(msg, debut, fin);
	}
	
	private void addEvent(String nom, Date debut, Date fin) {
		
		DefaultScheduleEvent event = new DefaultScheduleEvent(nom, debut, fin);
		event.setEditable(false);
        eventModel.addEvent(event);
	}
}


