package fr.pocolobo.afcepf.controller.rh;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.pocolobo.afcepf.business.api.IntervenantIBusiness;
import fr.pocolobo.afcepf.controller.AccessManager;
import fr.pocolobo.afcepf.controller.ConnexionBean;
import fr.pocolobo.afcepf.controller.NavigationBean;
import fr.pocolobo.afcepf.entity.utilisateurs.Intervenant;

@ManagedBean
@ViewScoped
public class ListeIntervenantsBean {

	private List<Intervenant> listeIntervenants;

	@EJB
	private IntervenantIBusiness itvBu;

	@ManagedProperty(value = "#{mbConnect}")
	private ConnexionBean mbConnect;

	@PostConstruct
	public void init() {
		listeIntervenants = itvBu.getAll();
	}

	// =====================================================
	// Restriction d'accès

	public String checkAccess() {

		AccessManager.Profile p = AccessManager.Profile.administration();
		if (!AccessManager.canAccess(mbConnect.getUtilisateur(), p)) {
			return NavigationBean.forbiddenUrl;
		}

		return null;
	}

	// =====================================================
	// Recherche

	private String champs;

	public String getChamps() {
		return champs;
	}

	public void setChamps(String champs) {
		this.champs = champs;
	}

	public void rechercher() {
		if (champs == null) {
			setListeIntervenants(itvBu.getAll());
			return;
		}

		listeIntervenants = itvBu.rechercherParAttributContenu("coordonnees.nom coordonnees.prenom", champs);
	}

	// =====================================================
	// Getters/setters

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}

	public List<Intervenant> getListeIntervenants() {
		return listeIntervenants;
	}

	public void setListeIntervenants(List<Intervenant> listeIntervenants) {
		this.listeIntervenants = listeIntervenants;
	}

	
}