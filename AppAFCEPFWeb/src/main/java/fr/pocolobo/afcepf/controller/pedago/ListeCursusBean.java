package fr.pocolobo.afcepf.controller.pedago;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.pocolobo.afcepf.business.api.CursusIBusiness;
import fr.pocolobo.afcepf.business.api.PromotionIBusiness;
import fr.pocolobo.afcepf.controller.AccessManager;
import fr.pocolobo.afcepf.controller.ConnexionBean;
import fr.pocolobo.afcepf.controller.NavigationBean;
import fr.pocolobo.afcepf.entity.pedago.Cursus;
import fr.pocolobo.afcepf.entity.pedago.Promotion;

@ManagedBean
@ViewScoped
public class ListeCursusBean {
	
	private List<Cursus> listeCursus;

	@EJB
	private CursusIBusiness cursusBu;
	
	@EJB
	private PromotionIBusiness promoBu;

	@ManagedProperty(value = "#{mbConnect}")
	private ConnexionBean mbConnect;

	//=====================================================

	@ManagedProperty(value = "#{editionPromotionBean}")
	private EditionPromotionBean editPromoBean;
	
	private Cursus cursusDeploy;
	
	private String messageErreurDeploy;
	
	private Date dateDeploy;
	
	private int numeroPromoDeploy;
	
	//=====================================================
	
	@PostConstruct
	public void init() {
		listeCursus = cursusBu.getAll();
	}
	
	public String deployer() {
		messageErreurDeploy = "";
		
		if (cursusDeploy == null) {
			messageErreurDeploy = "Veuillez choisir un cursus à déployer.";
			return null;
		}
		
		if (dateDeploy == null) {
			messageErreurDeploy = "Veuillez choisir une date à partir de laquelle déployer le cursus.";
			return null;
		}
		
		if (numeroPromoDeploy < 0) {
			messageErreurDeploy = "Veuillez choisir un numéro de promotion valide.";
			return null;
		}
		
		Promotion p = promoBu.creerPromotion(cursusDeploy, dateDeploy, numeroPromoDeploy);
		
		return editPromoBean.afficher(p);
	}
	
	private void refreshDeploiement() {
		List<Promotion> promos = promoBu.getAllPourCursus(cursusDeploy.getId());
		
		int biggestNumero = 0;
		
		for (Promotion p : promos) {
			if (p.getNumero() > biggestNumero) {
				biggestNumero = p.getNumero();
			}
		}
		
		numeroPromoDeploy = biggestNumero + 1;
		
		dateDeploy = new Date();
	}

	// =====================================================
	// Restriction d'accès

	public String checkAccess() {

		AccessManager.Profile p = AccessManager.Profile.administration();
		if (!AccessManager.canAccess(mbConnect.getUtilisateur(), p)) {
			return NavigationBean.forbiddenUrl;
		}

		return null;
	}

	// =====================================================
	// Recherche

	private String champs;

	public String getChamps() {
		return champs;
	}

	public void setChamps(String champs) {
		this.champs = champs;
	}

	public void rechercher() {
		if (champs == null) {
			setListeCursus(cursusBu.getAll());
			return;
		}

		listeCursus = cursusBu.rechercherParAttributContenu("nom", champs);
	}

	// =====================================================
	// Getters/setters

	public List<Cursus> getListeCursus() {
		return listeCursus;
	}

	public void setListeCursus(List<Cursus> listeCursus) {
		this.listeCursus = listeCursus;
	}

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}

	public Cursus getCursusDeploy() {
		return cursusDeploy;
	}

	public void setCursusDeploy(Cursus cursusDeploy) {
		this.cursusDeploy = cursusDeploy;
		
		refreshDeploiement();
	}

	public String getMessageErreurDeploy() {
		return messageErreurDeploy;
	}

	public void setMessageErreurDeploy(String messageErreurDeploy) {
		this.messageErreurDeploy = messageErreurDeploy;
	}

	public Date getDateDeploy() {
		return dateDeploy;
	}

	public void setDateDeploy(Date dateDeploy) {
		this.dateDeploy = dateDeploy;
	}

	public int getNumeroPromoDeploy() {
		return numeroPromoDeploy;
	}

	public void setNumeroPromoDeploy(int numeroPromoDeploy) {
		this.numeroPromoDeploy = numeroPromoDeploy;
	}

	public EditionPromotionBean getEditPromoBean() {
		return editPromoBean;
	}

	public void setEditPromoBean(EditionPromotionBean editPromoBean) {
		this.editPromoBean = editPromoBean;
	}
	
}