package fr.pocolobo.afcepf.controller.pedago;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import fr.pocolobo.afcepf.business.api.CursusIBusiness;
import fr.pocolobo.afcepf.business.api.ModuleIBusiness;
import fr.pocolobo.afcepf.controller.AccessManager;
import fr.pocolobo.afcepf.controller.ConnexionBean;
import fr.pocolobo.afcepf.controller.NavigationBean;
import fr.pocolobo.afcepf.entity.pedago.Cursus;

@ManagedBean
@SessionScoped
public class EditionCursusBean implements Serializable {

	private static final String thisPage = "editionCursus?faces-redirect=true";
	
	private static final long serialVersionUID = 1L;
	
	private Cursus cursusEdite;
	
	@EJB
	private CursusIBusiness cursusBu;

	@EJB
	private ModuleIBusiness moduleBu;

    @ManagedProperty(value="#{mbConnect}")
    private ConnexionBean mbConnect;
    
	
	public String checkAccess() {

		AccessManager.Profile p = AccessManager.Profile.administration();		
		if (! AccessManager.canAccess(mbConnect.getUtilisateur(), p)) {
			return NavigationBean.forbiddenUrl;
		}
		
		return null;
	}

	public String afficher(Cursus c) {
		
		cursusEdite = c;
		cursusEdite.setModules(moduleBu.getAllForCursus(cursusEdite.getId()));
		
		return thisPage;
	}
	
	public String deplacerMatiereHaut(int matiereId) {
		return thisPage;
	}

	public String deplacerMatiereBas(int matiereId) {		
		return thisPage;
	}

	public String ajouterMatiere(int matiereId) {
		System.out.println("ajout matière...");
		
		// TODO cassé par l'ajout de la table MatiereModule
		/*
		for (Module m : cursusEdite.getModules()) {
			for (MatiereModule matmod : m.getMatiereModules()) {
				if (matmod.getMatiere().getId() == matiereId) {
					final int insertIndex = m.getMatiereModules().indexOf(matmod);
					
					
					m.getMatieres().add(insertIndex,mat);
					
					System.out.println("Matière ajoutée " + insertIndex);
					
					break;
				}
			}
		}
		*/
		
		return thisPage;
	}

	public String retirerMatiere(int matiereId) {

		// TODO cassé par l'ajout de la table MatiereModule
		/*
		for (Module m : cursusEdite.getModules()) {
			for (Matiere mat : m.getMatieres()) {
				if (mat.getId() == matiereId) {
					final int insertIndex = m.getMatieres().indexOf(mat);
					m.getMatieres().remove(mat);
					
					System.out.println("Matière retirée " + insertIndex);
					
					break;
				}
			}
		}
		*/
		
		return thisPage;
	}
	

	//========================================================
	// Getters/Setters
	public Cursus getCursusEdite() {
		return cursusEdite;
	}

	public void setCursusEdite(Cursus cursusEdite) {
		this.cursusEdite = cursusEdite;
	}

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}
	
	
}