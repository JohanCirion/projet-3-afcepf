package fr.pocolobo.afcepf.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import fr.pocolobo.afcepf.business.api.CiviliteIBusiness;
import fr.pocolobo.afcepf.business.api.CoordonneesIBusiness;
import fr.pocolobo.afcepf.business.api.DossierCandidatureIBusiness;
import fr.pocolobo.afcepf.business.api.PromotionIBusiness;
import fr.pocolobo.afcepf.business.api.PromotionStagiaireIBusiness;
import fr.pocolobo.afcepf.business.api.StagiaireIBusiness;
import fr.pocolobo.afcepf.entity.Civilite;
import fr.pocolobo.afcepf.entity.DossierCandidature;
import fr.pocolobo.afcepf.entity.pedago.Promotion;
import fr.pocolobo.afcepf.entity.pedago.PromotionStagiaire;

@ManagedBean(name = "editionCandidatureBean")
@SessionScoped
public class EditionCandidatureBean {

	private static final String thisPage = "/editionCandidature?faces-redirect=true";
	
	private DossierCandidature dossCand;
	
	private List<Civilite> civilites;
	
	private Promotion promoAffect;

	@EJB
	private CoordonneesIBusiness coordonnesBu;
	@EJB
	private DossierCandidatureIBusiness ibuCand;
	@EJB
	private CiviliteIBusiness ibuCivil;
	@EJB
	private PromotionIBusiness promoBu;
	@EJB
	private StagiaireIBusiness stagiaireBu;
	@EJB
	private PromotionStagiaireIBusiness promoStagiaireBu;
	
	@ManagedProperty(value = "#{alertManagerBean}")
	private AlertManagerBean alertManager;

	@ManagedProperty(value = "#{mbConnect}")
	private ConnexionBean mbConnect;

	@PostConstruct
	public void init() {
		civilites = ibuCivil.getAll();
		//System.out.println("Nb civilite : " + civilites.size());
		
	}
	
	public void enregistrer() { 

		coordonnesBu.modifier(dossCand.getStagiaire().getCoordonnees());
		stagiaireBu.modifier(dossCand.getStagiaire());
		ibuCand.modifier(dossCand);
		
		System.out.println("dossier modifié");
	}

	public String getTitrePage() {
		return ! mbConnect.isConnecte() ? "" : 
			(mbConnect.isStagiaire() ? "Compléter ma candidature" : "Gestion dossier de candidature");
	}
	
	public String affiche(DossierCandidature dc) {
		
		dossCand = dc;
		
		return thisPage;
	}
	
	public String getAvancement() {
		if (dossCand == null) {
			return "";
		}
		
		if (dossCand.isRefuse()) {
			return "Refusé";
		}
		
		if (dossCand.isAccepte()) {
			return "Accepté";
		}
		
		switch (dossCand.getAvancement()) {
		case attenteRI:
			return "En attente de réunion d'information";
		case attenteAffectation:
			return "En attente d'affectation";
		case initial:
			return "En cours";
		case attenteEI:
			return "En attente d'un entretin individuel";
		default:
			return "";
		}
	}
	
	public boolean isDossierClos() {
		return dossCand != null && (dossCand.isRefuse() || dossCand.isAccepte());
	}
	
	public String accepterDossier() {
		
		// Acceptation du dossier
		dossCand.setAccepte(true);
		ibuCand.modifier(dossCand);
		
		// Ajout affectation à la promotion
		PromotionStagiaire ps = new PromotionStagiaire();
		ps.setPromotion(promoAffect);
		ps.setStagiaire(dossCand.getStagiaire());
		promoStagiaireBu.ajouter(ps);
		
        alertManager.refresh();
		
		return thisPage;
	}
	
	public String refuserDossier() {
		dossCand.setRefuse(true);
		ibuCand.modifier(dossCand);

        alertManager.refresh();
        
		return thisPage;
	}
	
	public List<Promotion> getPromos() {
		if (dossCand == null)
			return new ArrayList<Promotion>();
		
		return promoBu.getAllPourCursus(dossCand.getCursusDemande().getId());
	}
	
	// =====================================================
	// Restriction d'accès

	public String checkAccess() {

		if (! mbConnect.isConnecte()) {
			return NavigationBean.forbiddenUrl;
		}

		return null;
	}
	// =====================================================

	public DossierCandidature getDossCand() {
		return dossCand;
	}


	public void setDossCand(DossierCandidature dossCand) {
		this.dossCand = dossCand;
	}

	public DossierCandidatureIBusiness getIbuCand() {
		return ibuCand;
	}

	public void setIbuCand(DossierCandidatureIBusiness ibuCand) {
		this.ibuCand = ibuCand;
	}

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}


	public List<Civilite> getCivilite() {
		return civilites;
	}


	public void setCivilite(List<Civilite> civilite) {
		this.civilites = civilite;
	}

	public CiviliteIBusiness getIbuCivil() {
		return ibuCivil;
	}

	public void setIbuCivil(CiviliteIBusiness ibuCivil) {
		this.ibuCivil = ibuCivil;
	}

	public List<Civilite> getCivilites() {
		return civilites;
	}

	public void setCivilites(List<Civilite> civilites) {
		this.civilites = civilites;
	}

	public Promotion getPromoAffect() {
		return promoAffect;
	}

	public void setPromoAffect(Promotion promoAffect) {
		this.promoAffect = promoAffect;
	}

	public AlertManagerBean getAlertManager() {
		return alertManager;
	}

	public void setAlertManager(AlertManagerBean alertManager) {
		this.alertManager = alertManager;
	}
	
	

}