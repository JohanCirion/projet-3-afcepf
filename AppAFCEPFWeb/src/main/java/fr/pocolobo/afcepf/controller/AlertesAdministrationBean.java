package fr.pocolobo.afcepf.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.pocolobo.afcepf.business.api.notification.NotificationIBusiness;
import fr.pocolobo.afcepf.entity.notification.Notification;

@ManagedBean
@ViewScoped
public class AlertesAdministrationBean {

	@EJB
	NotificationIBusiness notifBu;
	
	@ManagedProperty(value="#{alertManagerBean}")
	AlertManagerBean alertManager;
	
	List<Notification> all;
	
	@PostConstruct
	void init() {
		
		alertManager.refresh();
		
		all = notifBu.getAll();
	}
	
	public List<Notification> getActionsDossiers() {
		List<Notification> r = new ArrayList<Notification>();
		
		for (Notification n : all) {
			if (n.getType() == Notification.Type.dossierCandidature)
				r.add(n);
		}
		
		return r;
	}

	public List<Notification> getActionsPromos() {
		List<Notification> r = new ArrayList<Notification>();
		
		for (Notification n : all) {
			if (n.getType() == Notification.Type.promotion)
				r.add(n);
		}
		
		return r;
	}

	public AlertManagerBean getAlertManager() {
		return alertManager;
	}

	public void setAlertManager(AlertManagerBean alertManager) {
		this.alertManager = alertManager;
	}
	
	
}
