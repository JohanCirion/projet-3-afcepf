package fr.pocolobo.afcepf.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class AccueilBean implements Serializable {

	private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{mbConnect}")
    private ConnexionBean mbConnect;

	public String getMessageBienvenue() {
		
		return mbConnect.getUtilisateur() == null ? 
				"Bienvenue sur le portail numérique de l'EQL"
				: "Bonjour " + mbConnect.getNomAffiche();
	}

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}
	
	

}