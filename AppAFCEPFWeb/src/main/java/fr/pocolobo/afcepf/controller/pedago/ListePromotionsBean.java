package fr.pocolobo.afcepf.controller.pedago;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.pocolobo.afcepf.business.api.PromotionIBusiness;
import fr.pocolobo.afcepf.controller.AccessManager;
import fr.pocolobo.afcepf.controller.ConnexionBean;
import fr.pocolobo.afcepf.controller.NavigationBean;
import fr.pocolobo.afcepf.entity.pedago.Promotion;

@ManagedBean
@ViewScoped
public class ListePromotionsBean {
	
	private List<Promotion> listePromos;
	
	@EJB
	private PromotionIBusiness promoBu;

    @ManagedProperty(value="#{mbConnect}")
    private ConnexionBean mbConnect;
    
    private boolean showPassees = false;
    
    private boolean showEnCours = true;
    
    private boolean showFutures = true;

	@PostConstruct
	public void init() {
		//setListePromos(promoBu.getAll());
		rechercher();
	}
	
	public Date getDateDebut(Promotion p) {
		
		if (p == null)
			return null;
		
		return promoBu.getDateDebut(p.getIdPromotion());
	}
	
	public Date getDateFin(Promotion p) {
		
		if (p == null)
			return null;
		
		return promoBu.getDateFin(p.getIdPromotion());
	}

	//=====================================================
	// Restriction d'accès

	public String checkAccess() {

		AccessManager.Profile p = AccessManager.Profile.administration();		
		if (! AccessManager.canAccess(mbConnect.getUtilisateur(), p)) {
			return NavigationBean.forbiddenUrl;
		}
		
		return null;
	}	

	//=====================================================
	// Recherche

	private String champs;
	
	public String getChamps() {
		return champs;
	}

	public void setChamps(String champs) {
		this.champs = champs;
	}
	
	public void rechercher() {
		
		System.out.println("HEEEYY");
		
		if (champs == null) {
			setListePromos(promoBu.getAll());
			filtrer();
			return;
		}
		
		listePromos = promoBu.rechercherParAttributContenu("nom", champs);
		filtrer();
	}
	
	public String getEtat(Promotion p) {
		if (promoBu.isEnCours(p)) 	return "En cours";
		if (promoBu.isPasse(p)) 	return "Passée";
		if (promoBu.isFuture(p)) 	return "Future";
		
		return "";
	}
	
	private void filtrer() {
		
		// On ne filtre pas si tout est décoché
		if (! showEnCours && ! showPassees && ! showFutures)
			return;
		
		List<Promotion> listeFiltre = new ArrayList<Promotion>();
		
		for (Promotion p : listePromos) {
			if (showEnCours && promoBu.isEnCours(p)) {
				listeFiltre.add(p);
			} else if (showPassees && promoBu.isPasse(p)) {
				listeFiltre.add(p);
			} else if (showFutures && promoBu.isFuture(p)) {
				listeFiltre.add(p);
			}
			
			setListePromos(listeFiltre);
		}
	}

	//=====================================================
	// Getters/Setters

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}

	public List<Promotion> getListePromos() {
		return listePromos;
	}

	public void setListePromos(List<Promotion> listePromos) {
		this.listePromos = listePromos;
	}

	public boolean isShowPassees() {
		return showPassees;
	}

	public void setShowPassees(boolean showPassees) {
		this.showPassees = showPassees;
	}

	public boolean isShowEnCours() {
		return showEnCours;
	}

	public void setShowEnCours(boolean showEnCours) {
		this.showEnCours = showEnCours;
	}

	public boolean isShowFutures() {
		return showFutures;
	}

	public void setShowFutures(boolean showFutures) {
		this.showFutures = showFutures;
	}

	
}