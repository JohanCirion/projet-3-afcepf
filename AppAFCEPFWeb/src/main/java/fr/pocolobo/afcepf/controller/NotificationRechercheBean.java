package fr.pocolobo.afcepf.controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.pocolobo.afcepf.business.api.GenericIBusiness;
import fr.pocolobo.afcepf.business.api.notification.NotificationIBusiness;
import fr.pocolobo.afcepf.entity.notification.Notification;

@ManagedBean
@SessionScoped
public class NotificationRechercheBean extends GenericRechercheBean<Notification> {
	
	@EJB
	private NotificationIBusiness bu;

	@Override
	protected String getRechercheAttribut() {
		return "shortMessage";
	}
	
	@Override
	protected GenericIBusiness<Notification> getBusiness() {
		return bu;
	}
	
	public GenericIBusiness<Notification> getBu() {
		return bu;
	}

	public void setBu(NotificationIBusiness bu) {
		this.bu = bu;
	}
}
