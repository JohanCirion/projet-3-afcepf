package fr.pocolobo.afcepf.controller;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.pocolobo.afcepf.business.api.CiviliteIBusiness;
import fr.pocolobo.afcepf.business.api.CoordonneesIBusiness;
import fr.pocolobo.afcepf.business.api.UtilisateurIBusiness;
import fr.pocolobo.afcepf.entity.Adresse;
import fr.pocolobo.afcepf.entity.Civilite;
import fr.pocolobo.afcepf.entity.Coordonnees;

@ManagedBean(name = "mbMonCompte")
@ViewScoped
public class MonCompteBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	static final private String thisPage = "/moncompte";

	private String message;
	private Coordonnees coordonnees;
	private List<Civilite> civilites;

	@ManagedProperty(value="#{mbConnect}")
	private ConnexionBean mbConnect;

	@EJB
	private UtilisateurIBusiness ibuUtilisateur;

	@EJB
	private CoordonneesIBusiness ibuCoord;

	@EJB
	private CiviliteIBusiness ibuCivil;
	
	@PostConstruct
	public void init() {
		
		civilites = ibuCivil.getAll(); 
		
		if (mbConnect.isConnecte()) {			
			copierCoordonnees();
		}
	}
	
	public void boutonDemo() {
		coordonnees.setPrenom("Julien");
		coordonnees.setNom("Civange");
		coordonnees.getAdresse().setAdresse("8, rue Marguerite");
		coordonnees.getAdresse().setVille("VIRY-CHÂTILLON");
		coordonnees.getAdresse().setCodePostal("91170");
		coordonnees.getAdresse().setPays("France");
		coordonnees.setTelephone("0123435441");
		coordonnees.setEmail("j.civange@supermail.org");
		
		Calendar c = new GregorianCalendar();
		c.set(1989, 3, 4);
		coordonnees.setDateNaissance(c.getTime());
		
		mbConnect.getUtilisateur().setCoordonnees(coordonnees);
	}
	
	public String checkAccess() {
	
		if (! mbConnect.isConnecte()) {
			return NavigationBean.forbiddenUrl;
		}
		
		return null;
	}

	private void copierCoordonnees() {

		Coordonnees orig = mbConnect.getUtilisateur().getCoordonnees();
		coordonnees = ibuCoord.rechercherParId(orig.getId());
		
		// Rustine pour éviter de travailler avec une adresse nulle
		if (coordonnees.getAdresse() == null) {
			coordonnees.setAdresse(new Adresse());
		}
	}


	public String enregistrer() {
		

		ibuCoord.modifier(mbConnect.getUtilisateur().getCoordonnees());

		
		return thisPage;

	}


	public void annuler() {
		message ="";			
		copierCoordonnees();


	}

	//Getter et Setter :

	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public ConnexionBean getMbConnect() {
		return mbConnect;
	}


	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}


	public CoordonneesIBusiness getIbuCoord() {
		return ibuCoord;
	}


	public void setIbuCoord(CoordonneesIBusiness ibuCoord) {
		this.ibuCoord = ibuCoord;
	}
	public Coordonnees getCoordonnees() {
		return coordonnees;
	}


	public void setCoordonnees(Coordonnees coordonnees) {
		this.coordonnees = coordonnees;
	}

	public List<Civilite> getCivilites() {
		return civilites;
	}

	public void setCivilites(List<Civilite> civilites) {
		this.civilites = civilites;
	}

	public CiviliteIBusiness getIbuCivil() {
		return ibuCivil;
	}

	public void setIbuCivil(CiviliteIBusiness ibuCivil) {
		this.ibuCivil = ibuCivil;
	}

	
	public UtilisateurIBusiness getIbuUtilisateur() {
		return ibuUtilisateur;
	}

	public void setIbuUtilisateur(UtilisateurIBusiness ibuUtilisateur) {
		this.ibuUtilisateur = ibuUtilisateur;
	}
	

}
