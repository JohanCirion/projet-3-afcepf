package fr.pocolobo.afcepf.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import fr.pocolobo.afcepf.business.api.CursusIBusiness;
import fr.pocolobo.afcepf.business.api.DossierCandidatureIBusiness;
import fr.pocolobo.afcepf.business.api.PromotionIBusiness;
import fr.pocolobo.afcepf.business.api.notification.NotificationIBusiness;
import fr.pocolobo.afcepf.controller.Warning.WarningType;
import fr.pocolobo.afcepf.entity.DossierCandidature;
import fr.pocolobo.afcepf.entity.notification.Notification;
import fr.pocolobo.afcepf.entity.notification.Notification.Type;
import fr.pocolobo.afcepf.entity.pedago.Cursus;
import fr.pocolobo.afcepf.entity.pedago.Promotion;

@ManagedBean
@ApplicationScoped
public class AlertManagerBean {

	private Set<Warning> warnings;
	
	@EJB
	private NotificationIBusiness notificationBu;
	
	@EJB
	private DossierCandidatureIBusiness dcBu;

	@EJB
	private PromotionIBusiness promoBu;
	
	@EJB
	private CursusIBusiness cursusBu;
	
	@PostConstruct
	public void init() {
		warnings = new HashSet<Warning>();
		
		refresh();
	}
	
	public void refresh() {
		lookForNewWarnings();
		resolveWarnings();
		notifyAdministration();
	}
	
	private void lookForNewWarnings() {
		
		// Dossiers candidatures en attente
		List<DossierCandidature> all = dcBu.getAll();
		
		for (DossierCandidature dc : all) {
			if (! dc.isAccepte() || ! dc.isRefuse()) {
				Warning w = new Warning(WarningType.actionCandidature, dc.getId().toString());
				warnings.add(w);
			}
		}
		
		// Promos à pourvoir
		List<Promotion> allPromos = promoBu.getAll();
		
		for (Promotion p : allPromos) {
			if (promoBu.getAllStagiaires(p.getIdPromotion()).size() < p.getCapaciteMin()) {
				Warning w = new Warning(WarningType.pourvoirPromo, p.getIdPromotion().toString());
				warnings.add(w);
			}
		}
	}
	
	private void resolveWarnings() {
		
		for (Warning w : warnings) {
			
			if (w.resolved)
				continue;
			
			if (w.type == WarningType.actionCandidature) {
				
				DossierCandidature dc = dcBu.rechercherParId(Integer.parseInt(w.data));
				
				w.resolved = (dc == null) 
						|| (dc.isAccepte() || dc.isRefuse());
				
			} else if (w.type == WarningType.pourvoirPromo) {
				
				Promotion p = promoBu.rechercherParId(Integer.parseInt(w.data));
				
				w.resolved = (p == null) 
						|| (promoBu.getAllStagiaires(p.getIdPromotion()).size() >= p.getCapaciteMin());
			}
		}
	}
	
	private void notifyAdministration() {
		List<Notification> all = notificationBu.getAll();
		
		for (Notification n : all) {
			notificationBu.supprimer(n);
		}
		
		HashMap<String, Integer> cursusCount = new HashMap<>();
		
		for (Warning w : warnings) {
			
			if (w.resolved)
				continue;
			
			if (w.type == WarningType.actionCandidature) {
				DossierCandidature dc = dcBu.rechercherParId(Integer.parseInt(w.data));
				Cursus c = cursusBu.rechercherParId(dc.getCursusDemande().getId());
				
				if (cursusCount.containsKey(c.getCode()))
					cursusCount.put(c.getCode(), cursusCount.get(c.getCode()) + 1);
				else {
					cursusCount.put(c.getCode(), 1);
				}
			} else if (w.type == WarningType.pourvoirPromo) {
				Promotion p = promoBu.rechercherParId(Integer.parseInt(w.data));
				
				String nomPromo = p.getCursus().getCode() + p.getNumero();
				
				notificationBu.notifierAdministration("Il faut recruter pour la promotion " + nomPromo + ".", Type.promotion, "promotions");				
			}
		}
		
		for (String c : cursusCount.keySet()) {
			notificationBu.notifierAdministration(cursusCount.get(c) + " candidature(s) pour le cursus " + c + ".", Type.dossierCandidature, "listeCandidatures");
		}
	}
}
