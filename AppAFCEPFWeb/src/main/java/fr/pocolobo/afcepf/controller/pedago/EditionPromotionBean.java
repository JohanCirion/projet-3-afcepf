package fr.pocolobo.afcepf.controller.pedago;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import fr.pocolobo.afcepf.business.api.CoursIBusiness;
import fr.pocolobo.afcepf.business.api.PromotionIBusiness;
import fr.pocolobo.afcepf.controller.AccessManager;
import fr.pocolobo.afcepf.controller.ConnexionBean;
import fr.pocolobo.afcepf.controller.NavigationBean;
import fr.pocolobo.afcepf.entity.pedago.Cours;
import fr.pocolobo.afcepf.entity.pedago.Promotion;
import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

@ManagedBean
@SessionScoped
public class EditionPromotionBean implements Serializable {

	private static final String thisPage = "editionPromotion?faces-redirect=true";
	
	private static final long serialVersionUID = 1L;
	
	private Promotion promoEdite;
	
	private ScheduleModel eventModel;
	
	@EJB
	private PromotionIBusiness promoBu;

	@EJB
	private CoursIBusiness coursBu;

    @ManagedProperty(value="#{mbConnect}")
    private ConnexionBean mbConnect;
    
    private ScheduleEvent event;
    
    private Cours coursSelection;

	//=========================================================================
    
    @PostConstruct
    public void init() {
		eventModel = new DefaultScheduleModel();	
    }
    
    public void onEventSelect(SelectEvent selectEvent) {
    	
        event = (ScheduleEvent) selectEvent.getObject();
        
        try {
        	Cours c = (Cours)event.getDynamicProperties().get("cours");
        	coursSelection = c;
        	
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }
    
    public Date getDateDebut() {
    	return promoBu.getDateDebut(promoEdite.getIdPromotion());
    }
    
    public Date getDateFin() {
    	return promoBu.getDateFin(promoEdite.getIdPromotion());
    }
    
    public void enregistrer() {
    	promoEdite = promoBu.modifier(promoEdite);
    }
    
    public void annuler() {
    	promoEdite = promoBu.rechercherParId(promoEdite.getIdPromotion());
    	
    }
    
	//=========================================================================
    
	public String checkAccess() {

		AccessManager.Profile p = AccessManager.Profile.administration();
		
		if (! AccessManager.canAccess(mbConnect.getUtilisateur(), p)) {
			return NavigationBean.forbiddenUrl;
		}
		
		return null;
	}

	public String afficher(Promotion p) {
		
		promoEdite = p;
		
		loadCours();
		
		return thisPage;
	}
	
	public List<Stagiaire> getStagiaires() {
		return promoBu.getAllStagiaires(promoEdite.getIdPromotion());
	}

	//=========================================================================
	
	public void  loadCours() {

		eventModel.clear();
		
		List<Cours> coursPromo = coursBu.getAllPourPromo(promoEdite.getIdPromotion());
		
		for (Cours c : coursPromo) {
			addCours(c);
		}
		
	}
	
	private void addCours(Cours c) {
		
		String intervenant = c.getIntervenant() == null ? "Non encadré" : c.getIntervenant().getCoordonnees().getNom();
		String msg = c.getMatiere().getNom() + " (" + intervenant + ")";

		for (int i = 0; i < c.getDureeBlocs(); ++i) {
			addDemiJournee(msg, c.getJour(), c.getBlocHoraire() + i == 1, c);
		}
	}
	
	private void addDemiJournee(String msg, Date jour, boolean apresMidi, Cours c) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(jour);
        cal.set(Calendar.HOUR_OF_DAY, apresMidi ? 14 : 0);
        cal.set(Calendar.MINUTE, 0);
		
        Date debut = cal.getTime();
		
        cal.set(Calendar.HOUR_OF_DAY, apresMidi ? 17 : 12);
        cal.set(Calendar.MINUTE, 30);
        
		Date fin = cal.getTime(); 
		
		addEvent(msg, debut, fin, c);
	}
	
	private void addEvent(String nom, Date debut, Date fin, Cours cours) {
		
		DefaultScheduleEvent event = new DefaultScheduleEvent(nom, debut, fin);
		event.setDynamicProperty("cours", cours);
		
		event.setEditable(false);
        eventModel.addEvent(event);
	}

	//=========================================================================
	// Getters/Setters
	
	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public ConnexionBean getMbConnect() {
		return mbConnect;
	}

	public void setMbConnect(ConnexionBean mbConnect) {
		this.mbConnect = mbConnect;
	}

	public Promotion getPromoEdite() {
		return promoEdite;
	}

	public void setPromoEdite(Promotion promoEdite) {
		this.promoEdite = promoEdite;
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}

	public Cours getCoursSelection() {
		return coursSelection;
	}

	public void setCoursSelection(Cours coursSelection) {
		this.coursSelection = coursSelection;
	}
	
}