package fr.pocolobo.afcepf.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.DossierCandidatureIDao;
import fr.pocolobo.afcepf.entity.DossierCandidature;

@Remote(DossierCandidatureIDao.class)
@Stateless
public class DossierCandidatureDao extends GenericDao<DossierCandidature> 
									implements DossierCandidatureIDao{

	@Override
	public String getDefaultOrderBy() {
		return "ORDER BY t.dateDepot";
	}
}
