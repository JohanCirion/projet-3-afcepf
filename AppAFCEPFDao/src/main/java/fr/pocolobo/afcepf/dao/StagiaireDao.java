package fr.pocolobo.afcepf.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.StagiaireIDao;
import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

@Remote(StagiaireIDao.class)
@Stateless
public class StagiaireDao extends GenericDao<Stagiaire> implements StagiaireIDao{

	@Override
	public String getDefaultOrderBy() {
		return "ORDER BY t.coordonnees.nom";
	}

}
