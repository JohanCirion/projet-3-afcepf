package fr.pocolobo.afcepf.dao;

import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.MatiereIntervenantIDao;
import fr.pocolobo.afcepf.entity.pedago.MatiereIntervenant;

@Stateless
public class MatiereIntervenantDao extends GenericDao<MatiereIntervenant> implements MatiereIntervenantIDao {
}
