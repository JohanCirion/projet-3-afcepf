package fr.pocolobo.afcepf.dao;

import java.util.List;

import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.CoursIDao;
import fr.pocolobo.afcepf.entity.pedago.Cours;

@Stateless
public class CoursDao extends GenericDao<Cours> implements CoursIDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Cours> getAllPourPromo(int idPromo) {

		return getEntityManager()
				.createQuery("SELECT t FROM Cours t WHERE t.promotion.id = " + idPromo + " ORDER BY t.jour, t.blocHoraire")
				.getResultList();
	}
}
