package fr.pocolobo.afcepf.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.IntervenantIDao;
import fr.pocolobo.afcepf.entity.pedago.Cours;
import fr.pocolobo.afcepf.entity.utilisateurs.Intervenant;

@Stateless
public class IntervenantDao extends GenericDao<Intervenant> implements IntervenantIDao {

	@Override
	public String getDefaultOrderBy() {
		return "ORDER BY t.coordonnees.nom";
	}
	
	@Override
	public List<Cours> getAllCours(int intervenantId) {
		Intervenant itv = rechercherParId(intervenantId);
		
		if (itv != null) {
			return itv.getListeCours();
		}
		
		return null;
	}
	
	@Override
	public boolean hasCours(int intervId, Date jour, int blocHoraire) {
		
		List<Cours> all = getAllCours(intervId);
		
		for (Cours c : all) {
			if (c.getJour() == jour) {
				if (c.getBlocHoraire() == blocHoraire)
					return true;
				
				if (c.getBlocHoraire() + c.getDureeBlocs() >= blocHoraire)
					return true;
			}
		}
		
		return false;
		
	}
	
}
