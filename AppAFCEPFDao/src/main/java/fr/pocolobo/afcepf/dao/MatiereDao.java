package fr.pocolobo.afcepf.dao;

import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.MatiereIDao;
import fr.pocolobo.afcepf.entity.pedago.Matiere;

@Stateless
public class MatiereDao extends GenericDao<Matiere> implements MatiereIDao {
}
