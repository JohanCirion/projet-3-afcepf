package fr.pocolobo.afcepf.dao;

import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.PromotionIDao;
import fr.pocolobo.afcepf.entity.pedago.Promotion;

@Stateless
public class PromotionDao extends GenericDao<Promotion> implements PromotionIDao {

	@Override
	public String getDefaultOrderBy() {
		return "ORDER BY t.cursus.nom";
	}
}
