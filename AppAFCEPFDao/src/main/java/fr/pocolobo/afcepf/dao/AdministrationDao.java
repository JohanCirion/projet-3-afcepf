package fr.pocolobo.afcepf.dao;

import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.AdministrationIDao;
import fr.pocolobo.afcepf.entity.utilisateurs.Administration;

@Stateless
public class AdministrationDao extends GenericDao<Administration> implements AdministrationIDao {
}
