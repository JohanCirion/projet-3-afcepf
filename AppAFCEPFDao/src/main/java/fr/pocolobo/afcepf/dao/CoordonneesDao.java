package fr.pocolobo.afcepf.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.pocolobo.afcepf.dao.api.CoordonneesIDao;
import fr.pocolobo.afcepf.entity.Coordonnees;

@Remote(CoordonneesIDao.class)
@Stateless
public class CoordonneesDao extends GenericDao<Coordonnees> implements CoordonneesIDao{

}
