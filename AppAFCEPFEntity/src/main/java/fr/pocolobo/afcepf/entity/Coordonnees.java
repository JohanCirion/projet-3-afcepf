package fr.pocolobo.afcepf.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import fr.pocolobo.afcepf.entity.utilisateurs.Utilisateur;

@Entity
@Table (name="coordonnees")
public class Coordonnees implements Serializable {
	
	private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private Integer id;
	
	private String prenom;
	private String nom;
	
	private String email;
	private String telephone;
	private Date dateNaissance;
	private String photo;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Adresse adresse;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Civilite civilite;
	
	@ManyToOne(fetch = FetchType.EAGER)
    private SituationFamilliale situationFamilliale;

	@OneToOne(mappedBy="coordonnees")
	private Utilisateur utilisateur;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public SituationFamilliale getSituationFamilliale() {
		return situationFamilliale;
	}

	public void setSituationFamilliale(SituationFamilliale situationFamilliale) {
		this.situationFamilliale = situationFamilliale;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Coordonnees(Integer id, String prenom, String nom, String email, String telephone, Date dateNaissance,
			String photo, Adresse adresse, Civilite civilite,
			SituationFamilliale situationFamilliale, Utilisateur utilisateur) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.email = email;
		this.telephone = telephone;
		this.dateNaissance = dateNaissance;
		this.photo = photo;
		this.adresse = adresse;
		this.civilite = civilite;
		this.situationFamilliale = situationFamilliale;
		this.utilisateur = utilisateur;
	}

	public Coordonnees() {
		super();
		// TODO Auto-generated constructor stub
	}

	
}
