package fr.pocolobo.afcepf.entity.pedago;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "promotion")
public class Promotion implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idPromotion;
	
	@OneToMany(mappedBy = "promotion")
	private List<PromotionStagiaire> promotionStagiaires;
	
	private int numero;
	
	@OneToMany(mappedBy = "promotion")
	private List<Cours> listeCours;
	
	@ManyToOne
	private Cursus cursus;
	
	private int capaciteMax;
	
	private int capaciteMin;

	//================================================================
	// Getter/Setters
	
	public Integer getIdPromotion() {
		return idPromotion;
	}

	public void setIdPromotion(Integer idPromotion) {
		this.idPromotion = idPromotion;
	}

	public List<PromotionStagiaire> getPromotionStagiaires() {
		return promotionStagiaires;
	}

	public void setPromotionStagiaires(List<PromotionStagiaire> promotionStagiaires) {
		this.promotionStagiaires = promotionStagiaires;
	}

	public List<Cours> getListeCours() {
		return listeCours;
	}

	public void setListeCours(List<Cours> cours) {
		this.listeCours = cours;
	}

	public Cursus getCursus() {
		return cursus;
	}

	public void setCursus(Cursus cursus) {
		this.cursus = cursus;
	}

	public int getCapaciteMax() {
		return capaciteMax;
	}

	public void setCapaciteMax(int capaciteMax) {
		this.capaciteMax = capaciteMax;
	}

	public int getCapaciteMin() {
		return capaciteMin;
	}

	public void setCapaciteMin(int capaciteMin) {
		this.capaciteMin = capaciteMin;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}	

}
