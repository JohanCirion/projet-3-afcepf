package fr.pocolobo.afcepf.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fr.pocolobo.afcepf.entity.pedago.Cursus;
import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

@Entity
@Table (name="dossiercandidature")
public class DossierCandidature implements Serializable {
	
	public enum Avancement {
		initial, attenteRI, attenteEI, attenteAffectation
	}
	
	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private Integer id;
	
    @ManyToOne
	private Cursus cursusDemande;
	
	private String niveauDiplome;
	
	private String cv;
	
	private String copiesDiplome;
	
	private String copieCNI;
	
	private Boolean benefRSA;
	
	private String identifiantPoleEmploi;
	
	private Boolean congeCIF;
	
	private Boolean professionnalisation;
	
	private Boolean demandeurEmploi;
	

	
	//=====================================================================
	
	private Date dateDepot;
	
	// Dossier terminé, stagiaire accepté
	private boolean accepte;
	
	// Dossier terminé, candidat refusé
	private boolean refuse;

	// Phase actuelle du dossier
	private Avancement avancement;

	//=====================================================================
	
	@OneToMany(mappedBy="dossierCandidature", fetch = FetchType.EAGER)
    private List<ConnaissanceInformatique> connaissanceInformatiques;
	
	@ManyToOne
	private Stagiaire stagiaire;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Cursus getCursusDemande() {
		return cursusDemande;
	}

	public void setCursusDemande(Cursus cursusDemande) {
		this.cursusDemande = cursusDemande;
	}

	public String getNiveauDiplome() {
		return niveauDiplome;
	}

	public void setNiveauDiplome(String niveauDiplome) {
		this.niveauDiplome = niveauDiplome;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public String getCopiesDiplome() {
		return copiesDiplome;
	}

	public void setCopiesDiplome(String copiesDiplome) {
		this.copiesDiplome = copiesDiplome;
	}

	public String getCopieCNI() {
		return copieCNI;
	}

	public void setCopieCNI(String copieCNI) {
		this.copieCNI = copieCNI;
	}

	public Boolean getBenefRSA() {
		return benefRSA;
	}

	public void setBenefRSA(Boolean benefRSA) {
		this.benefRSA = benefRSA;
	}

	public String getIdentifiantPoleEmploi() {
		return identifiantPoleEmploi;
	}

	public void setIdentifiantPoleEmploi(String identifiantPoleEmploi) {
		this.identifiantPoleEmploi = identifiantPoleEmploi;
	}

	public List<ConnaissanceInformatique> getConnaissanceInformatiques() {
		return connaissanceInformatiques;
	}

	public void setConnaissanceInformatiques(List<ConnaissanceInformatique> connaissanceInformatiques) {
		this.connaissanceInformatiques = connaissanceInformatiques;
	}

	public Stagiaire getStagiaire() {
		return stagiaire;
	}

	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}

	public DossierCandidature(Integer id, Cursus cursusDemande, String niveauDiplome, String cv, String copiesDiplome,
			String copieCNI, Boolean benefRSA, String identifiantPoleEmploi,
			List<ConnaissanceInformatique> connaissanceInformatiques, Stagiaire stagiaire, Boolean congeCIF, Boolean professionnalisation, Boolean demandeurEmploi) {
		super();
		this.id = id;
		this.cursusDemande = cursusDemande;
		this.niveauDiplome = niveauDiplome;
		this.cv = cv;
		this.copiesDiplome = copiesDiplome;
		this.copieCNI = copieCNI;
		this.benefRSA = benefRSA;
		this.identifiantPoleEmploi = identifiantPoleEmploi;
		this.connaissanceInformatiques = connaissanceInformatiques;
		this.stagiaire = stagiaire;
		this.congeCIF = congeCIF;
		this.professionnalisation = professionnalisation;
		this.demandeurEmploi = demandeurEmploi;
	}

	public Boolean getCongeCIF() {
		return congeCIF;
	}

	public void setCongeCIF(Boolean congeCIF) {
		this.congeCIF = congeCIF;
	}

	public Boolean getProfessionnalisation() {
		return professionnalisation;
	}

	public void setProfessionnalisation(Boolean professionnalisation) {
		this.professionnalisation = professionnalisation;
	}

	public Boolean getDemandeurEmploi() {
		return demandeurEmploi;
	}

	public void setDemandeurEmploi(Boolean demandeurEmploi) {
		this.demandeurEmploi = demandeurEmploi;
	}

	public DossierCandidature() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean isAccepte() {
		return accepte;
	}

	public void setAccepte(boolean accepte) {
		this.accepte = accepte;
	}

	public boolean isRefuse() {
		return refuse;
	}

	public void setRefuse(boolean refuse) {
		this.refuse = refuse;
	}

	public Avancement getAvancement() {
		return avancement;
	}

	public void setAvancement(Avancement avancement) {
		this.avancement = avancement;
	}

	public Date getDateDepot() {
		return dateDepot;
	}

	public void setDateDepot(Date dateDepot) {
		this.dateDepot = dateDepot;
	}

	
	
}
