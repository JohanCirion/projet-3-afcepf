package fr.pocolobo.afcepf.entity.pedago;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "module")
public class Module implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	private String nom;

	@ManyToOne(fetch = FetchType.LAZY)
	private Cursus cursus;

	@OneToMany(mappedBy = "module", fetch = FetchType.EAGER)
	private List<MatiereModule> matiereModules;
	
	private int ordre;

	//================================================================
	// Getter/Setters
	
	public Module() {
		super();
	}
	
	public Module(String nom) {
		this.nom = nom;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Cursus getCursus() {
		return cursus;
	}

	public void setCursus(Cursus cursus) {
		this.cursus = cursus;
	}

	public List<MatiereModule> getMatiereModules() {
		return matiereModules;
	}

	public void setMatiereModules(List<MatiereModule> matiereModules) {
		this.matiereModules = matiereModules;
	}

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}
	
}
