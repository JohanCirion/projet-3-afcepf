package fr.pocolobo.afcepf.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="civilite")
public class Civilite implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private int id;
	
	private String libelle;
	
	@OneToMany(mappedBy="civilite")
	private List<Coordonnees> coordonnees;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Coordonnees> getCoordonnees() {
		return coordonnees;
	}

	public void setCoordonnees(List<Coordonnees> coordonnees) {
		this.coordonnees = coordonnees;
	}

	public Civilite(int id, String libelle, List<Coordonnees> coordonnees) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.coordonnees = coordonnees;
	}

	public Civilite() {
		super();
		// TODO Auto-generated constructor stub
	}

	
}
