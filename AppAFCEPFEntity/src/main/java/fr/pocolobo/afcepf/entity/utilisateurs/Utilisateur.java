package fr.pocolobo.afcepf.entity.utilisateurs;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import fr.pocolobo.afcepf.entity.Coordonnees;

@Entity
@Inheritance
@DiscriminatorColumn(name="utilisateurType")
@Table (name = "utilisateur")
public abstract class Utilisateur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "login")
	private String login;

	@Column(name = "password")
	private String password;

	@OneToOne(fetch = FetchType.EAGER)
	private Coordonnees coordonnees;

	//    @OneToMany (mappedBy = "utilisateur", cascade =CascadeType.ALL, fetch = FetchType.EAGER)
	//private Set<AlerteIndisponibilite> alertes;

	public enum ProfileType {
		stagiaire, intervenant, administrateur
	}
	
	abstract public ProfileType getProfile();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Utilisateur() {
		super();
	}

	public Utilisateur(Integer id, String login, String password, Coordonnees coordonnees) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.coordonnees = coordonnees;
	}


	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", login=" + login + ", password=" + password + "]";
	}

	public Coordonnees getCoordonnees() {
		return coordonnees;
	}

	public void setCoordonnees(Coordonnees coordonnees) {
		this.coordonnees = coordonnees;
	}

}
