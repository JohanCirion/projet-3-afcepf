package fr.pocolobo.afcepf.entity.pedago;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "matierecursus")
public class ModuleCursus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//classe intermédiaire pour éviter de faire le manyToMany
	
	// Pierre : Pas convaincu de l'utilité de cette classe. 
	// Entre Cursus et Module, il devrait y avoir un OneToMany (plusieurs modules appartiennent à un cursus
	// mais un module n'appartient qu'à un seul cursus) car il me semble qu'il y a peu de chances pour qu'un module
	// soit réutilisé dans un autre cursus avec les mêmes matières.
	// Attention au nom de table ("matierecursus")
	

}
