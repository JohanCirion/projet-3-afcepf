package fr.pocolobo.afcepf.entity.utilisateurs;

public class UtilisateurHelper {
	
	public static boolean isStagiaire(Utilisateur utilisateur) {

		boolean resultat = false;
		
		try {
			@SuppressWarnings("unused")
			Stagiaire s = (Stagiaire) utilisateur;
			resultat = true;
		} catch (ClassCastException e) {
			//e.printStackTrace();
		}

		return resultat;
	}

	public static boolean isIntervenant(Utilisateur utilisateur) {

		boolean resultat = false;
		
		try {
			@SuppressWarnings("unused")
			Intervenant i = (Intervenant) utilisateur;
			resultat = true;
		} catch (ClassCastException e) {
			//e.printStackTrace();
		}

		return resultat;
	}

	public static boolean isAdministration(Utilisateur utilisateur) {

		boolean resultat = false;
		
		try {
			@SuppressWarnings("unused")
			Administration a = (Administration) utilisateur;
			resultat = true;
		} catch (ClassCastException e) {
			//e.printStackTrace();
		}

		return resultat;
	}
}
