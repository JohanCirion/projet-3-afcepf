package fr.pocolobo.afcepf.entity.utilisateurs;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import fr.pocolobo.afcepf.entity.pedago.PromotionStagiaire;

@Entity
@DiscriminatorValue("stagiaire")
public class Stagiaire extends Utilisateur {
		
	private static final long serialVersionUID = 1L;
	
	private Integer idStagiaire;	// Pierre : à quoi ça sert ?
	
	@OneToMany(mappedBy = "stagiaire")
	private List<PromotionStagiaire> promotionStagiaires;
	
	@Override
	public ProfileType getProfile() {
		return ProfileType.stagiaire;
	}
	
	public Integer getIdStagiaire() {
		return idStagiaire;
	}

	public void setIdStagiaire(Integer idStagiaire) {
		this.idStagiaire = idStagiaire;
	}

	public void signaler() {
        // TODO implement here
    }

	public Stagiaire(int idStagiaire) {
		super();
		this.idStagiaire = idStagiaire;
	}

	public Stagiaire() {
		super();
	}

	@Override
	public String toString() {
		return "Stagiaire [idStagiaire=" + idStagiaire + "]";
	}

	public List<PromotionStagiaire> getPromotionStagiaires() {
		return promotionStagiaires;
	}

	public void setPromotionStagiaires(List<PromotionStagiaire> promotionStagiaires) {
		this.promotionStagiaires = promotionStagiaires;
	}

}
