package fr.pocolobo.afcepf.entity;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name="connaissanceinformatique")
public class ConnaissanceInformatique implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private Integer id;
	
	private String langage;
	
	private int niveau;
	
	@ManyToOne
	private DossierCandidature dossierCandidature;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLangage() {
		return langage;
	}

	public void setLangage(String langage) {
		this.langage = langage;
	}

	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	public DossierCandidature getDossierCandidature() {
		return dossierCandidature;
	}

	public void setDossierCandidature(DossierCandidature dossierCandidature) {
		this.dossierCandidature = dossierCandidature;
	}

	public ConnaissanceInformatique(Integer id, String langage, int niveau, DossierCandidature dossierCandidature) {
		super();
		this.id = id;
		this.langage = langage;
		this.niveau = niveau;
		this.dossierCandidature = dossierCandidature;
	}

	public ConnaissanceInformatique() {
		super();
		// TODO Auto-generated constructor stub
	}


	
	

}
