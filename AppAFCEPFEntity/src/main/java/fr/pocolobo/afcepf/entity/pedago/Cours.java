package fr.pocolobo.afcepf.entity.pedago;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fr.pocolobo.afcepf.entity.utilisateurs.Intervenant;

@Entity
@Table(name = "cours")
public class Cours implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@ManyToOne
	private Matiere matiere;

	@ManyToOne
	private Promotion promotion;
	
	private Date jour;
	
	private int blocHoraire;
	
	private int dureeBlocs;
	
	@ManyToOne
	private Intervenant intervenant;

	//================================================================
	// Getter/Setters
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public Date getJour() {
		return jour;
	}

	public void setJour(Date jour) {
		this.jour = jour;
	}

	public int getBlocHoraire() {
		return blocHoraire;
	}

	public void setBlocHoraire(int blocHoraire) {
		this.blocHoraire = blocHoraire;
	}

	public int getDureeBlocs() {
		return dureeBlocs;
	}

	public void setDureeBlocs(int dureeBlocs) {
		this.dureeBlocs = dureeBlocs;
	}

	public Intervenant getIntervenant() {
		return intervenant;
	}

	public void setIntervenant(Intervenant intervenant) {
		this.intervenant = intervenant;
	}
	
}
