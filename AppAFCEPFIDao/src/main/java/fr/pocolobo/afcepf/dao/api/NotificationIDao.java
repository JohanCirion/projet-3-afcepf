package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.notification.Notification;

public interface NotificationIDao extends GenericIDao<Notification> {

}
