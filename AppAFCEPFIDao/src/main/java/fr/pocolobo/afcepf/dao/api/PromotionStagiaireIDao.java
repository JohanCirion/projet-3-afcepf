package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.pedago.PromotionStagiaire;

public interface PromotionStagiaireIDao extends GenericIDao<PromotionStagiaire> {
}
