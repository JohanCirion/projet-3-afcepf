package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.Civilite;

public interface CiviliteIDao extends GenericIDao<Civilite> {

}
