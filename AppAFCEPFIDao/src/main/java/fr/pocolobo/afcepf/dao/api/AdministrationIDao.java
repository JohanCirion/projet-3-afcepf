package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.utilisateurs.Administration;

public interface AdministrationIDao extends GenericIDao<Administration> {
}
