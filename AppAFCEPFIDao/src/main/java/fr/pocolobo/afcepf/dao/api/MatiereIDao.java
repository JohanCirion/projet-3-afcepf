package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.pedago.Matiere;

public interface MatiereIDao extends GenericIDao<Matiere> {
}
