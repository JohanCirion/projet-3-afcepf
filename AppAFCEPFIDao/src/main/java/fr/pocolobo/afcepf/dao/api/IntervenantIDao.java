package fr.pocolobo.afcepf.dao.api;

import java.util.Date;
import java.util.List;

import fr.pocolobo.afcepf.entity.pedago.Cours;
import fr.pocolobo.afcepf.entity.utilisateurs.Intervenant;

public interface IntervenantIDao extends GenericIDao<Intervenant> {

	List<Cours> getAllCours(int intervenantId);
	
	boolean hasCours(int intervId, Date jour, int blocHoraire);
}
