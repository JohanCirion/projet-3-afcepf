package fr.pocolobo.afcepf.dao.api;

import fr.pocolobo.afcepf.entity.utilisateurs.Stagiaire;

public interface StagiaireIDao extends GenericIDao<Stagiaire>{
}
