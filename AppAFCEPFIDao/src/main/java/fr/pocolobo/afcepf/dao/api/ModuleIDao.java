package fr.pocolobo.afcepf.dao.api;

import java.util.List;

import fr.pocolobo.afcepf.entity.pedago.Module;

public interface ModuleIDao extends GenericIDao<Module> {
	
	List<Module> getAllForCursus(int idCursus);
	
}
