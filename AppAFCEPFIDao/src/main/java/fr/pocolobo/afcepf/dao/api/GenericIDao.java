package fr.pocolobo.afcepf.dao.api;

import java.util.List;

public interface GenericIDao<T> {
    T ajouter(T t);
    boolean supprimer (T t);
    T modifier(T t);
    T rechercherParId (Integer i);
	List<T> getAll();
	
	List<T> rechercherParAttributEntier (String nomAttribut, int valeur);
	List<T> rechercherParAttributContenu (String nomAttribut, String valeur);
}
